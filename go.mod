module advent-of-code-2022

go 1.19

replace advent-of-code-2020 => gitlab.com/kurisuchan/advent-of-code-2020 v0.0.0-20201225055828-b67a782fc982

require (
	advent-of-code-2020 v0.0.0-00010101000000-000000000000
	golang.org/x/exp v0.0.0-20221208152030-732eee02a75a
)
