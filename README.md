This repository is a collection of my solutions for the [Advent of Code 2022](http://adventofcode.com/2022) calendar.

| Puzzle                                        | Silver                 | Gold                   |
|-----------------------------------------------|------------------------|------------------------|
| [Day 1: Calorie Counting](pkg/day01)          | `    >24h`  (`166333`) | `    >24h`  (`160034`) |
| [Day 2: Rock Paper Scissors](pkg/day02)       | `    >24h`  (`130119`) | `    >24h`  (`124340`) |
| [Day 3: Rucksack Reorganization](pkg/day03)   | `12:37:38`  (` 75547`) | `12:45:02`  (` 69110`) |
| [Day 4: Camp Cleanup](pkg/day04)              | `03:47:49`  (` 30554`) | `03:51:58`  (` 29307`) |
| [Day 5: Supply Stacks](pkg/day05)             | `16:04:54`  (` 75723`) | `16:16:45`  (` 73790`) |
| [Day 6: Tuning Trouble](pkg/day06)            | `11:20:53`  (` 72633`) | `11:22:58`  (` 71289`) |
| [Day 7: No Space Left On Device](pkg/day07)   | `02:09:52`  (` 12232`) | `02:18:06`  (` 11428`) |
| [Day 8: Treetop Tree House](pkg/day08)        | `02:52:21`  (` 19846`) | `03:03:55`  (` 16153`) |
| [Day 9: Rope Bridge](pkg/day09)               | `10:44:07`  (` 38411`) | `10:52:17`  (` 29225`) |
| [Day 10: Cathode-Ray Tube](pkg/day10)         | `05:11:52`  (` 23765`) | `05:49:31`  (` 21360`) |
| [Day 11: Monkey in the Middle](pkg/day11)     | `09:00:01`  (` 28274`) | `09:12:21`  (` 20523`) |
| [Day 12: Hill Climbing Algorithm](pkg/day12)  | `13:50:56`  (` 26123`) | `13:55:18`  (` 25013`) |
| [Day 13: Distress Signal](pkg/day13)          | `17:14:28`  (` 28276`) | `17:32:07`  (` 27237`) |
| [Day 14: Regolith Reservoir](pkg/day14)       | `02:57:25`  (`  9375`) | `03:01:25`  (`  8480`) |
| [Day 15: Beacon Exclusion Zone](pkg/day15)    | `04:57:55`  (` 11972`) | `06:04:06`  (`  8229`) |
| [Day 16: Proboscidea Volcanium](pkg/day16)    | `14:26:18`  (` 10385`) | `16:15:56`  (`  6749`) |
| [Day 17: Pyroclastic Flow](pkg/day17)         | `11:12:12`  (`  9682`) | `    >24h`  (` 16689`) |
| [Day 18: Boiling Boulders](pkg/day18)         | `09:34:39`  (` 13394`) | `09:51:57`  (`  9627`) |
| [Day 19: Not Enough Minerals](pkg/day19)      | `02:23:21`  (`  1475`) | `02:41:00`  (`  1153`) |
| [Day 20: Grove Positioning System](pkg/day20) | `09:35:18`  (`  8491`) | `09:41:39`  (`  7630`) |
| [Day 21: Monkey Math](pkg/day21)              | `05:31:02`  (`  9944`) | `06:27:53`  (`  7573`) |
| [Day 22: Monkey Map](pkg/day22)               | `04:53:44`  (`  5595`) | `09:02:42`  (`  3269`) |
| [Day 23: Unstable Diffusion](pkg/day23)       | `03:12:51`  (`  4040`) | `03:19:24`  (`  3886`) |
| [Day 24: Blizzard Basin](pkg/day24)           | `05:59:00`  (`  4511`) | `06:03:48`  (`  4212`) |
| [Day 25: Full of Hot Air](pkg/day25)          | `01:37:50`  (`  3316`) | `02:29:40`  (`  3090`) |

^1: This year I'm not starting as soon as the next puzzle is available, so start times are completely arbitrary.
