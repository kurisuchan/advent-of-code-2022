package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day10"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("SignalStrength: %d\n", day10.SignalStrength(input))
	fmt.Printf("CRTDraw:\n%s", day10.CRTDraw(input))
}
