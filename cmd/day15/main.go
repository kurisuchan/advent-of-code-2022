package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day15"
)

const bounds = 4000000

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("NoBeacons:      %d\n", day15.NoBeacons(input, bounds))
	fmt.Printf("DistressBeacon: %d\n", day15.DistressBeacon(input, bounds))
}
