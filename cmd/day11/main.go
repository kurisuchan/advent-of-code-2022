package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day11"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("MonkeyBusiness:      %d\n", day11.MonkeyBusiness(input))
	fmt.Printf("RidiculouslyWorried: %d\n", day11.RidiculouslyWorried(input))
}
