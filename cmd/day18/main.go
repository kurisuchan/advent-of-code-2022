package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day18"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("SurfaceArea:     %d\n", day18.SurfaceArea(input))
	fmt.Printf("ExteriorSurface: %d\n", day18.ExteriorSurface(input))
}
