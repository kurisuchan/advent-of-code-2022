package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day19"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("BlueprintQuality: %d\n", day19.BlueprintQuality(input))
	fmt.Printf("HungryElephants:  %d\n", day19.HungryElephants(input))
}
