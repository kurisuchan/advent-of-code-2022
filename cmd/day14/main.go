package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day14"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("SandOverflow: %d\n", day14.SandOverflow(input))
	fmt.Printf("SandPile:     %d\n", day14.SandPile(input))
}
