package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day02"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("Strategy:        %d\n", day02.Strategy(input))
	fmt.Printf("CorrectStrategy: %d\n", day02.CorrectStrategy(input))
}
