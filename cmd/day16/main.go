package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day16"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("PressureRelease: %d\n", day16.PressureRelease(input))
	fmt.Printf("ElephantRelease: %d\n", day16.ElephantRelease(input))
}
