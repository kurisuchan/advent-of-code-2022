package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day01"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("Calories:    %d\n", day01.Calories(input))
	fmt.Printf("TopCalories: %d\n", day01.TopCalories(input))
}
