package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day05"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("CrateMover9000: %s\n", day05.CrateMover9000(input))
	fmt.Printf("CrateMover9001: %s\n", day05.CrateMover9001(input))
}
