package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day09"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("ShortRope: %d\n", day09.ShortRope(input))
	fmt.Printf("LongRope:  %d\n", day09.LongRope(input))
}
