package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day07"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("TotalSize: %d\n", day07.TotalSize(input))
	fmt.Printf("FreeSpace: %d\n", day07.FreeSpace(input))
}
