package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day06"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("start-of-packet:  %d\n", day06.FindUnique(input, day06.StartOfPacket))
	fmt.Printf("start-of-message: %d\n", day06.FindUnique(input, day06.StartOfMessage))
}
