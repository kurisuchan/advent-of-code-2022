package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day03"
)

func main() {
	input := utils2020.ReadStringLinesFromFile("input.txt")

	fmt.Printf("Compartments: %d\n", day03.Compartments(input))
	fmt.Printf("Badges:       %d\n", day03.Badges(input))
}
