package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day08"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("VisibleTrees: %d\n", day08.VisibleTrees(input))
	fmt.Printf("ScenicView:   %d\n", day08.ScenicView(input))
}
