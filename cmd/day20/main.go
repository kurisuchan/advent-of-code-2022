package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day20"
)

func main() {
	input := utils2020.ReadIntLinesFromFile("input.txt")

	fmt.Printf("Mix:     %d\n", day20.Mix(input))
	fmt.Printf("Decrypt: %d\n", day20.Decrypt(input))
}
