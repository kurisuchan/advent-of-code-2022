package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day04"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("Contained:  %d\n", day04.Contained(input))
	fmt.Printf("Overlapped: %d\n", day04.Overlapped(input))
}
