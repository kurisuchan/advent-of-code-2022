package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day22"
)

const faceSize = 50

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("MonkeyPassword: %d\n", day22.MonkeyPassword(input))
	fmt.Printf("CubePassword:   %d\n", day22.CubePassword(input, faceSize, day22.RealInputTarget))
}
