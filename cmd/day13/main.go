package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day13"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("SignalPairs:    %d\n", day13.SignalPairs(input))
	fmt.Printf("DistressSignal: %d\n", day13.DistressSignal(input))
}
