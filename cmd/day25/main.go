package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day25"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("Bob: %s\n", day25.Bob(input))
}
