package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day23"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("EmptyTiles: %d\n", day23.EmptyTiles(input))
	fmt.Printf("SpreadOut:  %d\n", day23.SpreadOut(input))
}
