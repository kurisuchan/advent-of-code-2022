package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day12"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("HillClimb:   %d\n", day12.HillClimb(input))
	fmt.Printf("ScenicClimb: %d\n", day12.ScenicClimb(input))
}
