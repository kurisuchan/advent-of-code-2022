package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day17"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("TowerOfRocks: %d\n", day17.TowerOfRocks(input))
	fmt.Printf("ManyRocks:    %d\n", day17.ManyRocks(input))
}
