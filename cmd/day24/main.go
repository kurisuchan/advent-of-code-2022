package main

import (
	"fmt"

	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day24"
)

func main() {
	input := utils2020.ReadStringFromFile("input.txt")

	fmt.Printf("BlizzardValley: %d\n", day24.BlizzardValley(input))
	fmt.Printf("SnackTrack:     %d\n", day24.SnackTrack(input))
}
