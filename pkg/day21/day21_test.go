package day21_test

import (
	"testing"

	"advent-of-code-2022/pkg/day21"
)

func TestMonkeyMath(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"root: pppw + sjmn\ndbpl: 5\ncczh: sllz + lgvd\nzczc: 2\nptdq: humn - dvpt\ndvpt: 3\nlfqf: 4\nhumn: 5\nljgn: 2\nsjmn: drzm * dbpl\nsllz: 4\npppw: cczh / lfqf\nlgvd: ljgn * ptdq\ndrzm: hmdt - zczc\nhmdt: 32\n", 152},
	}

	for _, test := range tests {
		actual := day21.MonkeyMath(test.in)
		if actual != test.out {
			t.Errorf("MonkeyMath(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestHumanMath(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"root: pppw + sjmn\ndbpl: 5\ncczh: sllz + lgvd\nzczc: 2\nptdq: humn - dvpt\ndvpt: 3\nlfqf: 4\nhumn: 5\nljgn: 2\nsjmn: drzm * dbpl\nsllz: 4\npppw: cczh / lfqf\nlgvd: ljgn * ptdq\ndrzm: hmdt - zczc\nhmdt: 32", 301},
		{"root: aaaa - bbbb\naaaa: 4\nbbbb: cccc - humn\ncccc: 6\nhumn: 0", 2},
		{"root: aaaa - bbbb\naaaa: 4\nbbbb: cccc / humn\ncccc: 8\nhumn: 0", 2},
	}

	for _, test := range tests {
		actual := day21.HumanMath(test.in)
		if actual != test.out {
			t.Errorf("HumanMath(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
