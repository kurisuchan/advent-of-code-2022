package day21

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"strings"
)

func MonkeyMath(input string) int {
	monkeys := parseInput(input)
	return monkeys["root"].Calculate()
}

func HumanMath(input string) int {
	monkeys := parseInput(input)

	humanBranch, otherBranch := humanOther(monkeys["root"])
	otherResult := otherBranch.Calculate()

	for !humanBranch.IsHuman {
		var nextHumanBranch *Monkey
		nextHumanBranch, otherBranch = humanOther(humanBranch)

		switch humanBranch.Operation.(type) {
		case OperationAdd:
			otherResult = otherResult - otherBranch.Calculate()
		case OperationMul:
			otherResult = otherResult / otherBranch.Calculate()
		case OperationSub:
			if humanBranch.A == nextHumanBranch {
				otherResult = otherResult + otherBranch.Calculate()
			} else {
				otherResult = otherBranch.Calculate() - otherResult
			}
		case OperationDiv:
			if humanBranch.A == nextHumanBranch {
				otherResult = otherBranch.Calculate() * otherResult
			} else {
				otherResult = otherBranch.Calculate() / otherResult
			}
		}

		humanBranch = nextHumanBranch
	}

	return otherResult
}

func humanOther(branch *Monkey) (*Monkey, *Monkey) {
	if branch.A.HasHuman() {
		return branch.A, branch.B
	}
	return branch.B, branch.A
}

func parseInput(input string) map[string]*Monkey {
	monkeys := make(map[string]*Monkey)
	lines := strings.Split(input, "\n")
	for _, line := range lines {
		if line == "" {
			continue
		}

		parts := strings.Split(line, " ")
		name := strings.TrimSuffix(parts[0], ":")

		monkey := &Monkey{
			IsHuman: name == "humn",
		}

		if len(parts) == 2 {
			monkey.Number = utils2020.MustInt(parts[1])
		} else {
			switch parts[2] {
			case "+":
				monkey.Operation = OperationAdd(parts[2])
			case "-":
				monkey.Operation = OperationSub(parts[2])
			case "*":
				monkey.Operation = OperationMul(parts[2])
			case "/":
				monkey.Operation = OperationDiv(parts[2])
			}
		}

		monkeys[name] = monkey
	}

	for _, line := range lines {
		if line == "" {
			continue
		}

		parts := strings.Split(line, " ")
		name := strings.TrimSuffix(parts[0], ":")
		if len(parts) == 4 {
			monkeys[name].A = monkeys[parts[1]]
			monkeys[name].B = monkeys[parts[3]]
		}
	}

	return monkeys
}

type Monkey struct {
	Number    int
	A         *Monkey
	B         *Monkey
	Operation Operation
	IsHuman   bool
}

func (m *Monkey) Calculate() int {
	if m.Operation == nil {
		return m.Number
	}
	return m.Operation.Math(m.A, m.B)
}

func (m *Monkey) HasHuman() bool {
	if m.IsHuman {
		return true
	}
	if m.Operation == nil {
		return false
	}
	return m.A.HasHuman() || m.B.HasHuman()
}

type Operation interface {
	Math(a, b *Monkey) int
}

type OperationAdd string

func (o OperationAdd) Math(a, b *Monkey) int {
	return a.Calculate() + b.Calculate()
}

type OperationSub string

func (o OperationSub) Math(a, b *Monkey) int {
	return a.Calculate() - b.Calculate()
}

type OperationDiv string

func (o OperationDiv) Math(a, b *Monkey) int {
	return a.Calculate() / b.Calculate()
}

type OperationMul string

func (o OperationMul) Math(a, b *Monkey) int {
	return a.Calculate() * b.Calculate()
}
