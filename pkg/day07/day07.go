package day07

import (
	"advent-of-code-2020/pkg/utils"
	"strings"
)

const (
	filesystem = 70000000
	required   = 30000000
)

func TotalSize(input []string) int {
	root := parseInput(utils.NoEmptyStrings(input))

	return root.TotalSize()
}

func FreeSpace(input []string) int {
	root := parseInput(utils.NoEmptyStrings(input))

	needed := required - (filesystem - root.Size())

	return root.Free(needed)
}

func parseInput(lines []string) *Directory {
	root := mkdir(nil)
	cwd := root

	for i := 0; i < len(lines); i++ {
		switch lines[i][0:4] {
		case "$ cd":
			path := lines[i][5:]
			switch path {
			case "/":
				cwd = root
			case "..":
				cwd = cwd.parent
			default:
				cwd = cwd.dirs[path]
			}
		case "$ ls":
			for i += 1; i < len(lines) && lines[i][0] != '$'; i++ {
				parts := strings.Split(lines[i], " ")
				if parts[0] == "dir" {
					cwd.dirs[parts[1]] = mkdir(cwd)
				} else {
					cwd.files[parts[1]] = utils.MustInt(parts[0])
				}
			}
			i--
		}
	}

	return root
}

type Directory struct {
	parent *Directory
	dirs   map[string]*Directory
	files  map[string]int
}

func mkdir(parent *Directory) *Directory {
	return &Directory{
		parent: parent,
		dirs:   make(map[string]*Directory),
		files:  make(map[string]int),
	}
}

func (d *Directory) Size() int {
	var sum int

	for k := range d.files {
		sum += d.files[k]
	}

	for k := range d.dirs {
		sum += d.dirs[k].Size()
	}

	return sum
}

func (d *Directory) TotalSize() int {
	var sum int

	for k := range d.dirs {
		sum += d.dirs[k].TotalSize()
	}

	s := d.Size()
	if s <= 100000 {
		sum += s
	}

	return sum
}

func (d *Directory) Free(required int) int {
	var smallest int

	s := d.Size()
	if s >= required {
		smallest = s
	}

	for k := range d.dirs {
		f := d.dirs[k].Free(required)
		if f > 0 && (smallest == 0 || f < smallest) {
			smallest = f
		}
	}

	return smallest
}
