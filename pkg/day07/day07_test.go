package day07_test

import (
	"testing"

	"advent-of-code-2022/pkg/day07"
)

func TestTotalSize(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"$ cd /",
			"$ ls",
			"dir a",
			"14848514 b.txt",
			"8504156 c.dat",
			"dir d",
			"$ cd a",
			"$ ls",
			"dir e",
			"29116 f",
			"2557 g",
			"62596 h.lst",
			"$ cd e",
			"$ ls",
			"584 i",
			"$ cd ..",
			"$ cd ..",
			"$ cd d",
			"$ ls",
			"4060174 j",
			"8033020 d.log",
			"5626152 d.ext",
			"7214296 k",
		}, 95437},
	}

	for i, test := range tests {
		actual := day07.TotalSize(test.in)
		if actual != test.out {
			t.Errorf("TotalSize(%d) => %d, want %d", i, actual, test.out)
		}
	}
}

func TestFreeSpace(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"$ cd /",
			"$ ls",
			"dir a",
			"14848514 b.txt",
			"8504156 c.dat",
			"dir d",
			"$ cd a",
			"$ ls",
			"dir e",
			"29116 f",
			"2557 g",
			"62596 h.lst",
			"$ cd e",
			"$ ls",
			"584 i",
			"$ cd ..",
			"$ cd ..",
			"$ cd d",
			"$ ls",
			"4060174 j",
			"8033020 d.log",
			"5626152 d.ext",
			"7214296 k",
		}, 24933642},
	}

	for i, test := range tests {
		actual := day07.FreeSpace(test.in)
		if actual != test.out {
			t.Errorf("FreeSpace(%d) => %d, want %d", i, actual, test.out)
		}
	}
}
