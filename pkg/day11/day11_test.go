package day11_test

import (
	"testing"

	"advent-of-code-2022/pkg/day11"
)

func TestMonkeyBusiness(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Monkey 0:\n  Starting items: 79, 98\n  Operation: new = old * 19\n  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3\n\nMonkey 1:\n  Starting items: 54, 65, 75, 74\n  Operation: new = old + 6\n  Test: divisible by 19\n    If true: throw to monkey 2\n    If false: throw to monkey 0\n\nMonkey 2:\n  Starting items: 79, 60, 97\n  Operation: new = old * old\n  Test: divisible by 13\n    If true: throw to monkey 1\n    If false: throw to monkey 3\n\nMonkey 3:\n  Starting items: 74\n  Operation: new = old + 3\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1", 10605},
	}

	for _, test := range tests {
		actual := day11.MonkeyBusiness(test.in)
		if actual != test.out {
			t.Errorf("MonkeyBusiness(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestRidiculouslyWorried(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Monkey 0:\n  Starting items: 79, 98\n  Operation: new = old * 19\n  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3\n\nMonkey 1:\n  Starting items: 54, 65, 75, 74\n  Operation: new = old + 6\n  Test: divisible by 19\n    If true: throw to monkey 2\n    If false: throw to monkey 0\n\nMonkey 2:\n  Starting items: 79, 60, 97\n  Operation: new = old * old\n  Test: divisible by 13\n    If true: throw to monkey 1\n    If false: throw to monkey 3\n\nMonkey 3:\n  Starting items: 74\n  Operation: new = old + 3\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1", 2713310158},
	}

	for _, test := range tests {
		actual := day11.RidiculouslyWorried(test.in)
		if actual != test.out {
			t.Errorf("RidiculouslyWorried(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestModAdd(t *testing.T) {
	tests := []struct {
		old int
		val *int
		out int
	}{
		{1, intPtr(2), 3},
		{1, nil, 2},
	}

	for _, test := range tests {
		actual := day11.ModAdd(test.old, test.val)
		if actual != test.out {
			t.Errorf("ModAdd(%v, %v) => %d, want %d", test.old, test.val, actual, test.out)
		}
	}
}

func intPtr(a int) *int {
	return &a
}
