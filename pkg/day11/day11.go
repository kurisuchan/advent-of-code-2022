package day11

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/utils"
	"regexp"
	"sort"
	"strings"
)

func MonkeyBusiness(input string) int {
	monkeys := parseInput(input)

	inspected := make(map[int]int)

	for round := 0; round < 20; round++ {
		for i := range monkeys {
			for _, item := range monkeys[i].Items {
				inspected[i]++
				item = monkeys[i].Mod(item, monkeys[i].ModVal) / 3

				if item%monkeys[i].Test == 0 {
					monkeys[monkeys[i].True].Items = append(monkeys[monkeys[i].True].Items, item)
				} else {
					monkeys[monkeys[i].False].Items = append(monkeys[monkeys[i].False].Items, item)
				}
			}
			monkeys[i].Items = nil
		}
	}

	var out []int
	for i := range inspected {
		out = append(out, inspected[i])
	}
	sort.Sort(sort.Reverse(sort.IntSlice(out)))

	return out[0] * out[1]
}

func RidiculouslyWorried(input string) int {
	monkeys := parseInput(input)

	inspected := make(map[int]int)

	var d []int
	for i := range monkeys {
		d = append(d, monkeys[i].Test)
	}
	lcm := utils.LeastCommonMultipleMany(d...)

	for round := 0; round < 10000; round++ {
		for i := range monkeys {
			for _, item := range monkeys[i].Items {
				inspected[i]++
				item = monkeys[i].Mod(item, monkeys[i].ModVal) % lcm

				if item%monkeys[i].Test == 0 {
					monkeys[monkeys[i].True].Items = append(monkeys[monkeys[i].True].Items, item)
				} else {
					monkeys[monkeys[i].False].Items = append(monkeys[monkeys[i].False].Items, item)
				}
			}
			monkeys[i].Items = nil
		}
	}

	var out []int
	for i := range inspected {
		out = append(out, inspected[i])
	}
	sort.Sort(sort.Reverse(sort.IntSlice(out)))

	return out[0] * out[1]
}

func parseInput(input string) []*Monkey {
	obs := regexp.MustCompile(`Monkey (\d+):\n.*?items: ([^\n]+)\n.*?Operation: new = old (.) (\d+|old)\n.*?Test: divisible by (\d+)\n.*?to monkey (\d+)\n.*?to monkey (\d+)`)

	var monkeys []*Monkey

	for _, match := range obs.FindAllStringSubmatch(input, -1) {
		monkey := &Monkey{
			Test:  utils2020.MustInt(match[5]),
			True:  utils2020.MustInt(match[6]),
			False: utils2020.MustInt(match[7]),
		}
		for _, item := range strings.Split(match[2], ", ") {
			monkey.Items = append(monkey.Items, utils2020.MustInt(item))
		}
		switch match[3] {
		case "*":
			monkey.Mod = ModMul
		case "+":
			monkey.Mod = ModAdd
		}
		if match[4] != "old" {
			monkey.ModVal = utils.Ptr(utils2020.MustInt(match[4]))
		}
		monkeys = append(monkeys, monkey)
	}
	return monkeys
}

type Monkey struct {
	Items  []int
	Mod    Modifier
	ModVal *int
	Test   int
	True   int
	False  int
}

type Modifier func(old int, val *int) int

func ModMul(old int, val *int) int {
	if val != nil {
		return old * *val
	}
	return old * old
}

func ModAdd(old int, val *int) int {
	if val != nil {
		return old + *val
	}
	return old + old
}
