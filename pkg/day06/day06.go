package day06

const (
	StartOfPacket  = 4
	StartOfMessage = 14
)

func FindUnique(input string, count int) int {
	for offset := 0; offset <= len(input)-count; offset++ {
		chrs := map[byte]struct{}{}

		for i := 0; i < count; i++ {
			chrs[input[i+offset]] = struct{}{}
		}

		if len(chrs) == count {
			return offset + count
		}
	}
	return -1
}
