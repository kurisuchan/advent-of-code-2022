package day06_test

import (
	"testing"

	"advent-of-code-2022/pkg/day06"
)

func TestFindUnique(t *testing.T) {
	tests := []struct {
		in    string
		count int
		out   int
	}{
		// Packet
		{"mjqjpqmgbljsphdztnvjfqwrcgsmlb", day06.StartOfPacket, 7},
		{"bvwbjplbgvbhsrlpgdmjqwftvncz", day06.StartOfPacket, 5},
		{"nppdvjthqldpwncqszvftbrmjlhg", day06.StartOfPacket, 6},
		{"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", day06.StartOfPacket, 10},
		{"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", day06.StartOfPacket, 11},
		// Message
		{"mjqjpqmgbljsphdztnvjfqwrcgsmlb", day06.StartOfMessage, 19},
		{"bvwbjplbgvbhsrlpgdmjqwftvncz", day06.StartOfMessage, 23},
		{"nppdvjthqldpwncqszvftbrmjlhg", day06.StartOfMessage, 23},
		{"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", day06.StartOfMessage, 29},
		{"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", day06.StartOfMessage, 26},
		//
		{"abc", 4, -1},
		{"aaaaaaaa", 4, -1},
		{"abcd", 4, 4},
	}

	for _, test := range tests {
		actual := day06.FindUnique(test.in, test.count)
		if actual != test.out {
			t.Errorf("FindUnique(%q, %d) => %d, want %d", test.in, test.count, actual, test.out)
		}
	}
}
