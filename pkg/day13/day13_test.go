package day13_test

import (
	"testing"

	"advent-of-code-2022/pkg/day13"
)

func TestSignalPairs(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"[1,1,3,1,1]\n[1,1,5,1,1]\n\n[[1],[2,3,4]]\n[[1],4]\n\n[9]\n[[8,7,6]]\n\n[[4,4],4,4]\n[[4,4],4,4,4]\n\n[7,7,7,7]\n[7,7,7]\n\n[]\n[3]\n\n[[[]]]\n[[]]\n\n[1,[2,[3,[4,[5,6,7]]]],8,9]\n[1,[2,[3,[4,[5,6,0]]]],8,9]", 13},
		{"[null]\n[null]", 0}, // coverage \o/
	}

	for _, test := range tests {
		actual := day13.SignalPairs(test.in)
		if actual != test.out {
			t.Errorf("SignalPairs(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestDistressSignal(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"[1,1,3,1,1]\n[1,1,5,1,1]\n\n[[1],[2,3,4]]\n[[1],4]\n\n[9]\n[[8,7,6]]\n\n[[4,4],4,4]\n[[4,4],4,4,4]\n\n[7,7,7,7]\n[7,7,7]\n\n[]\n[3]\n\n[[[]]]\n[[]]\n\n[1,[2,[3,[4,[5,6,7]]]],8,9]\n[1,[2,[3,[4,[5,6,0]]]],8,9]", 140},
	}

	for _, test := range tests {
		actual := day13.DistressSignal(test.in)
		if actual != test.out {
			t.Errorf("DistressSignal(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
