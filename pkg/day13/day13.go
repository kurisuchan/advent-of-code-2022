package day13

import (
	"advent-of-code-2022/pkg/utils"
	"encoding/json"
	"fmt"
	"sort"
	"strings"
)

func SignalPairs(input string) int {
	packets := parseInput(input)
	var result int
	for i := 0; i < len(packets); i += 2 {
		correct, _ := rightOrder(packets[i], packets[i+1])
		if correct {
			result += i/2 + 1
		}
	}
	return result
}

func DistressSignal(input string) int {
	dividerA := "[[2]]"
	dividerB := "[[6]]"

	packets := parseInput(input)
	packets = append(packets, parseInput(dividerA)...)
	packets = append(packets, parseInput(dividerB)...)

	sort.Sort(packets)

	result := 1

	for i, p := range packets {
		pretty := fmt.Sprintf("%+v", p)
		if pretty == dividerA || pretty == dividerB {
			result *= i + 1
		}
	}
	return result
}

type PacketList [][]interface{}

func (p PacketList) Less(i, j int) bool {
	correct, _ := rightOrder(p[i], p[j])
	return correct
}

func (p PacketList) Len() int {
	return len(p)
}

func (p PacketList) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func parseInput(input string) PacketList {
	var list PacketList

	for _, line := range strings.Split(input, "\n") {
		if line == "" {
			continue
		}
		var p []interface{}
		_ = json.Unmarshal([]byte(line), &p)
		list = append(list, p)
	}

	return list
}

func intOrList(in interface{}) (*int, []interface{}) {
	switch i := in.(type) {
	case float64:
		return utils.Ptr(int(i)), nil
	case int:
		return &i, nil
	case []interface{}, []float64:
		return nil, i.([]interface{})
	default:
		return nil, nil
	}
}

func rightOrder(a, b interface{}) (correct bool, keepGoing bool) {
	aInt, aList := intOrList(a)
	bInt, bList := intOrList(b)

	if aInt != nil && bInt != nil {
		if *aInt < *bInt {
			correct = true
			keepGoing = false
		} else if *aInt > *bInt {
			correct = false
			keepGoing = false
		} else {
			correct = false
			keepGoing = true
		}
	} else if aList != nil && bInt != nil {
		correct, keepGoing = rightOrder(aList, []interface{}{*bInt})
	} else if bList != nil && aInt != nil {
		correct, keepGoing = rightOrder([]interface{}{*aInt}, bList)
	} else if len(aList) == 0 {
		if len(bList) == 0 {
			correct = false
			keepGoing = true
		} else {
			correct = true
			keepGoing = false
		}
	} else {
		var pos int
		for pos = 0; pos < len(aList) && pos < len(bList); pos++ {
			correct, keepGoing = rightOrder(aList[pos], bList[pos])
			if !keepGoing {
				return
			}
		}
		if len(aList) < len(bList) {
			correct = true
			keepGoing = false
		} else if len(bList) < len(aList) {
			correct = false
			keepGoing = false
		} else {
			correct = false
			keepGoing = true
		}
	}
	return
}
