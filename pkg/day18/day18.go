package day18

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/utils"
	"strings"
)

func SurfaceArea(input string) int {
	cubes := parseInput(input)
	var neighbors int
	for cube := range cubes {
		for n := range Coordinate3Neighbors6 {
			if _, found := cubes[cube.Move(Coordinate3Neighbors6[n])]; found {
				neighbors++
			}
		}
	}
	return len(cubes)*6 - neighbors
}

func ExteriorSurface(input string) int {
	cubes := parseInput(input)
	waters := make(map[utils2020.Coordinate3]struct{})
	var min, max utils2020.Coordinate3
	for cube := range cubes {
		min.X = utils.Min(cube.X-1, min.X)
		min.Y = utils.Min(cube.Y-1, min.Y)
		min.Z = utils.Min(cube.Z-1, min.Z)
		max.X = utils.Max(cube.X+1, max.X)
		max.Y = utils.Max(cube.Y+1, max.Y)
		max.Z = utils.Max(cube.Z+1, max.Z)
	}

	waters[min] = struct{}{}
	waters[max] = struct{}{}

	var settled bool
	for !settled {
		settled = true
		for water := range waters {
			for n := range Coordinate3Neighbors6 {
				pos := water.Move(Coordinate3Neighbors6[n])
				if !inBounds(min, max, pos) {
					continue
				}
				if _, found := waters[pos]; found {
					continue
				}
				if _, found := cubes[pos]; found {
					continue
				}

				waters[pos] = struct{}{}
				settled = false
			}
		}
	}

	var neighbors int
	for cube := range cubes {
		for n := range Coordinate3Neighbors6 {
			if _, found := waters[cube.Move(Coordinate3Neighbors6[n])]; found {
				neighbors++
			}
		}
	}
	return neighbors
}

func parseInput(input string) map[utils2020.Coordinate3]struct{} {
	cubes := make(map[utils2020.Coordinate3]struct{})
	for _, row := range strings.Split(input, "\n") {
		segments := strings.Split(row, ",")
		if len(segments) == 3 {
			coordinate := utils2020.Coordinate3{
				X: utils2020.MustInt(segments[0]),
				Y: utils2020.MustInt(segments[1]),
				Z: utils2020.MustInt(segments[2]),
			}
			cubes[coordinate] = struct{}{}
		}
	}
	return cubes
}

var Coordinate3Neighbors6 = [6]utils2020.Coordinate3{
	{X: -1, Y: 0, Z: 0},
	{X: 1, Y: 0, Z: 0},
	{X: 0, Y: -1, Z: 0},
	{X: 0, Y: 1, Z: 0},
	{X: 0, Y: 0, Z: -1},
	{X: 0, Y: 0, Z: 1},
}

func inBounds(min, max, pos utils2020.Coordinate3) bool {
	return min.X <= pos.X && pos.X <= max.X && min.Y <= pos.Y && pos.Y <= max.Y && min.Z <= pos.Z && pos.Z <= max.Z
}
