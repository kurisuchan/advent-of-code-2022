package day18_test

import (
	"testing"

	"advent-of-code-2022/pkg/day18"
)

func TestSurfaceArea(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"1,1,1\n2,1,1\n", 10},
		{"2,2,2\n1,2,2\n3,2,2\n2,1,2\n2,3,2\n2,2,1\n2,2,3\n2,2,4\n2,2,6\n1,2,5\n3,2,5\n2,1,5\n2,3,5", 64},
	}

	for _, test := range tests {
		actual := day18.SurfaceArea(test.in)
		if actual != test.out {
			t.Errorf("SurfaceArea(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestExteriorSurface(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"2,2,2\n1,2,2\n3,2,2\n2,1,2\n2,3,2\n2,2,1\n2,2,3\n2,2,4\n2,2,6\n1,2,5\n3,2,5\n2,1,5\n2,3,5", 58},
	}

	for _, test := range tests {
		actual := day18.ExteriorSurface(test.in)
		if actual != test.out {
			t.Errorf("ExteriorSurface(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
