package utils

import "golang.org/x/exp/constraints"

func Min[T constraints.Ordered](x, y T) T {
	if x < y {
		return x
	}
	return y
}

func Max[T constraints.Ordered](x, y T) T {
	if x > y {
		return x
	}
	return y
}

func GreatestCommonDivisor[T constraints.Integer](a, b T) T {
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

func LeastCommonMultiple[T constraints.Integer](a, b T) T {
	return a * b / GreatestCommonDivisor(a, b)
}

func LeastCommonMultipleMany[T constraints.Integer](args ...T) T {
	if len(args) == 2 {
		return LeastCommonMultiple(args[0], args[1])
	}
	return LeastCommonMultiple(args[0], LeastCommonMultipleMany(args[1:]...))
}
