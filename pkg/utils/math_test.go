package utils_test

import (
	"advent-of-code-2022/pkg/utils"
	"testing"
)

func TestMax(t *testing.T) {
	tests := []struct {
		a, b int
		out  int
	}{
		{54, 24, 54},
		{48, 180, 180},
		{13, 13, 13},
		{37, 600, 600},
		{20, 100, 100},
		{624129, 2061517, 2061517},
		{-1, 1, 1},
		{1, -1, 1},
	}

	for _, test := range tests {
		actual := utils.Max(test.a, test.b)
		if actual != test.out {
			t.Errorf("Max(%d, %d) => %d, want %d", test.a, test.b, actual, test.out)
		}
	}
}

func TestMin(t *testing.T) {
	tests := []struct {
		a, b int
		out  int
	}{
		{54, 24, 24},
		{48, 180, 48},
		{13, 13, 13},
		{37, 600, 37},
		{20, 100, 20},
		{624129, 2061517, 624129},
		{-1, 1, -1},
		{1, -1, -1},
	}

	for _, test := range tests {
		actual := utils.Min(test.a, test.b)
		if actual != test.out {
			t.Errorf("Min(%d, %d) => %d, want %d", test.a, test.b, actual, test.out)
		}
	}
}

func TestGreatestCommonDivisor(t *testing.T) {
	tests := []struct {
		a, b int
		out  int
	}{
		{54, 24, 6},
		{48, 180, 12},
		{13, 13, 13},
		{37, 600, 1},
		{20, 100, 20},
		{624129, 2061517, 18913},
	}

	for _, test := range tests {
		actual := utils.GreatestCommonDivisor(test.a, test.b)
		if actual != test.out {
			t.Errorf("GreatestCommonDivisor(%d, %d) => %d, want %d", test.a, test.b, actual, test.out)
		}
	}
}

func TestLeastCommonMultipleMany(t *testing.T) {
	tests := []struct {
		values []int
		out    int
	}{
		{[]int{120, 140}, 840},
		{[]int{10213, 312}, 3186456},
		{[]int{10, 30}, 30},
		{[]int{10, 20, 30, 40}, 120},
		{[]int{12, 15, 10, 75}, 300},
	}

	for _, test := range tests {
		actual := utils.LeastCommonMultipleMany(test.values...)
		if actual != test.out {
			t.Errorf("LeastCommonMultipleMany(%v) => %d, want %d", test.values, actual, test.out)
		}
	}
}
