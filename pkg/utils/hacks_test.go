package utils_test

import (
	"advent-of-code-2022/pkg/utils"
	"testing"
)

func TestPtr(t *testing.T) {
	tests := []struct {
		in any
	}{
		{123},
		{"foo"},
		{nil},
	}

	for _, test := range tests {
		actual := utils.Ptr(test.in)
		if *actual != test.in {
			t.Errorf("Ptr(%+v) => %v (%v), want %v (%v)", test.in, *actual, actual, test.in, &test.in)
		}
	}
}
