package day20_test

import (
	"testing"

	"advent-of-code-2022/pkg/day20"
)

func TestMix(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{1, 2, -3, 3, -2, 0, 4}, 3},
	}

	for _, test := range tests {
		actual := day20.Mix(test.in)
		if actual != test.out {
			t.Errorf("Mix(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestDecrypt(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{1, 2, -3, 3, -2, 0, 4}, 1623178306},
	}

	for _, test := range tests {
		actual := day20.Decrypt(test.in)
		if actual != test.out {
			t.Errorf("Decrypt(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
