package day20

import (
	"container/ring"
)

const decryptionKey = 811589153

func Mix(input []int) int {
	zero, order := parseInput(input)

	for _, pos := range order {
		list := pos.Prev()
		target := pos.Prev().Unlink(1)
		destination := list.Move(pos.Value.(int))
		destination.Link(target)
	}

	return getCoordinates(zero)
}

func Decrypt(input []int) int {
	zero, order := parseInput(input)
	for _, pos := range order {
		pos.Value = pos.Value.(int) * decryptionKey
	}

	for i := 0; i < 10; i++ {
		for _, pos := range order {
			list := pos.Prev()
			target := pos.Prev().Unlink(1)
			destination := list.Move(pos.Value.(int) % (len(order) - 1))
			destination.Link(target)
		}
	}

	return getCoordinates(zero)
}

func parseInput(input []int) (*ring.Ring, []*ring.Ring) {
	list := ring.New(len(input))
	var zero *ring.Ring
	order := make([]*ring.Ring, len(input))
	for i := range input {
		if input[i] == 0 {
			zero = list
		}
		list.Value = input[i]
		order[i] = list
		list = list.Next()
	}
	return zero, order
}

func getCoordinates(list *ring.Ring) int {
	var sum int
	for i := 0; i < 3; i++ {
		list = list.Move(1000)
		sum += list.Value.(int)
	}
	return sum
}
