package day12_test

import (
	"testing"

	"advent-of-code-2022/pkg/day12"
)

func TestHillClimb(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi", 31},
		{"Szz\nzzz\nzzE", 0}, // coverage \o/
	}

	for _, test := range tests {
		actual := day12.HillClimb(test.in)
		if actual != test.out {
			t.Errorf("HillClimb(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestScenicClimb(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi", 29},
	}

	for _, test := range tests {
		actual := day12.ScenicClimb(test.in)
		if actual != test.out {
			t.Errorf("ScenicClimb(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
