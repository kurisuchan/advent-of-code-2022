package day12

import "container/heap"

type Traversable interface {
	Neighbors() []Traversable
	NeighborCost(to Traversable) int
	EstimateCost(to Traversable) int
	Equal(to Traversable) bool
}

type Equality func(Traversable, Traversable) bool

type node struct {
	traversable Traversable
	cost        int
	rank        int
	parent      *node
	open        bool
	closed      bool
	index       int
}

type nodeMap map[Traversable]*node

func (nm nodeMap) get(t Traversable) *node {
	n, ok := nm[t]
	if !ok {
		n = &node{
			traversable: t,
		}
		nm[t] = n
	}
	return n
}

func Path(from, to Traversable) (path []Traversable, distance int, found bool) {
	nm := nodeMap{}
	nq := &priorityQueue{}
	heap.Init(nq)
	fromNode := nm.get(from)
	fromNode.open = true
	heap.Push(nq, fromNode)
	for {
		if nq.Len() == 0 {
			return
		}
		current := heap.Pop(nq).(*node)
		current.open = false
		current.closed = true
		if current.traversable.Equal(nm.get(to).traversable) {
			var p []Traversable
			curr := current
			for curr != nil {
				p = append(p, curr.traversable)
				curr = curr.parent
			}
			return p, current.cost, true
		}

		for _, neighbor := range current.traversable.Neighbors() {
			cost := current.cost + current.traversable.NeighborCost(neighbor)
			neighborNode := nm.get(neighbor)
			if cost < neighborNode.cost {
				if neighborNode.open {
					heap.Remove(nq, neighborNode.index)
				}
				neighborNode.open = false
				neighborNode.closed = false
			}
			if !neighborNode.open && !neighborNode.closed {
				neighborNode.cost = cost
				neighborNode.open = true
				neighborNode.rank = cost + neighbor.EstimateCost(to)
				neighborNode.parent = current
				heap.Push(nq, neighborNode)
			}
		}
	}
}

type priorityQueue []*node

func (pq priorityQueue) Len() int {
	return len(pq)
}

func (pq priorityQueue) Less(i, j int) bool {
	return pq[i].rank < pq[j].rank
}

func (pq priorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index, pq[j].index = i, j
}

func (pq *priorityQueue) Push(x interface{}) {
	n := len(*pq)
	no := x.(*node)
	no.index = n
	*pq = append(*pq, no)
}

func (pq *priorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	no := old[n-1]
	no.index = -1
	*pq = old[:n-1]
	return no
}
