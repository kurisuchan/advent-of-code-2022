package day12

import (
	"advent-of-code-2020/pkg/utils"
	"sort"
	"strings"
)

func HillClimb(input string) int {
	starts, exit := parseInput(input)
	_, distance, _ := Path(starts[0], exit)
	return distance
}

func ScenicClimb(input string) int {
	var distances []int
	starts, exit := parseInput(input)
	for i := range starts {
		_, distance, found := Path(starts[i], exit)
		if found {
			distances = append(distances, distance)
		}
	}
	sort.Ints(distances)
	return distances[0]
}

type Node struct {
	pos       utils.Coordinate2
	neighbors []Traversable
	elevation uint8
}

func (n *Node) Neighbors() []Traversable {
	return n.neighbors
}

func (n *Node) NeighborCost(_ Traversable) int {
	return 1
}

func (n *Node) EstimateCost(to Traversable) int {
	return n.pos.Distance(to.(*Node).pos)
}

func (n *Node) Equal(to Traversable) bool {
	return n == to.(*Node)
}

func parseInput(input string) ([]*Node, *Node) {
	out := make(map[utils.Coordinate2]*Node)
	var starts []*Node
	var exit *Node
	for y, line := range strings.Split(input, "\n") {
		for x := 0; x < len(line); x++ {
			pos := utils.Coordinate2{X: x, Y: y}
			n := &Node{
				elevation: line[x],
				pos:       pos,
			}
			switch line[x] {
			case 'S':
				starts = append([]*Node{n}, starts...)
				n.elevation = 'a'
			case 'a':
				starts = append(starts, n)
			case 'E':
				exit = n
				n.elevation = 'z'
			}
			out[pos] = n
		}
	}

	for pos, c := range out {
		for i := range utils.Coordinate2Neighbors4 {
			n, ok := out[pos.Move(utils.Coordinate2Neighbors4[i])]
			if !ok {
				continue
			}
			if n.elevation <= c.elevation+1 {
				c.neighbors = append(c.neighbors, n)
			}
		}
	}

	return starts, exit
}
