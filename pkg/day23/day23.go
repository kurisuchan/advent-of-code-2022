package day23

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/utils"
	"container/ring"
	"math"
	"strings"
)

func EmptyTiles(input string) int {
	grove := parseInput(input)
	moves := getMoves()

	for i := 0; i < 10; i++ {
		proposed := make(map[utils2020.Coordinate2]int)

		for pos := range grove {
			moved := noMove(pos, grove, proposed)
			for j := 0; j < 4 && !moved; j++ {
				moved = moves.Move(j).Value.(move)(pos, grove, proposed)
			}
		}

		newGrove := make(Scan)

		for oldPos, newPos := range grove {
			if proposed[newPos] == 1 {
				newGrove[newPos] = newPos
			} else {
				newGrove[oldPos] = oldPos
			}
		}

		grove = newGrove
		moves = moves.Next()
	}

	return grove.empty()
}

func SpreadOut(input string) int {
	grove := parseInput(input)
	moves := getMoves()

	var i int
	for {
		i++
		noElfMoved := true
		proposed := make(map[utils2020.Coordinate2]int)

		for pos := range grove {
			moved := noMove(pos, grove, proposed)
			for j := 0; j < 4 && !moved; j++ {
				moved = moves.Move(j).Value.(move)(pos, grove, proposed)
				if moved {
					noElfMoved = false
				}
			}
		}

		if noElfMoved {
			return i
		}

		newGrove := make(Scan)
		for oldPos, newPos := range grove {
			if proposed[newPos] == 1 {
				newGrove[newPos] = newPos
			} else {
				newGrove[oldPos] = oldPos
			}
		}

		grove = newGrove
		moves = moves.Next()
	}
}

func parseInput(input string) Scan {
	grove := make(Scan)
	for y, line := range strings.Split(input, "\n") {
		for x, c := range line {
			if c == '#' {
				grove[utils2020.Coordinate2{X: x, Y: y}] = utils2020.Coordinate2{X: x, Y: y}
			}
		}
	}

	return grove
}

type Scan map[utils2020.Coordinate2]utils2020.Coordinate2

func (s Scan) hasElf(pos utils2020.Coordinate2) bool {
	_, found := s[pos]
	return found
}

func (s Scan) empty() int {
	var min, max utils2020.Coordinate2
	min.X = math.MaxInt
	min.Y = math.MaxInt
	for pos := range s {
		min.X = utils.Min(min.X, pos.X)
		min.Y = utils.Min(min.Y, pos.Y)
		max.X = utils.Max(max.X, pos.X)
		max.Y = utils.Max(max.Y, pos.Y)
	}
	return (max.X-min.X+1)*(max.Y-min.Y+1) - len(s)
}

func getMoves() *ring.Ring {
	moves := ring.New(4)

	for _, m := range []move{moveNorth, moveSouth, moveWest, moveEast} {
		moves.Value = m
		moves = moves.Next()
	}

	return moves
}

type move func(utils2020.Coordinate2, Scan, map[utils2020.Coordinate2]int) bool

func noMove(pos utils2020.Coordinate2, grove Scan, proposed map[utils2020.Coordinate2]int) bool {
	for i := range utils2020.Coordinate2Neighbors8 {
		if grove.hasElf(pos.Move(utils2020.Coordinate2Neighbors8[i])) {
			return false
		}
	}

	proposed[pos]++
	return true
}

func moveNorth(pos utils2020.Coordinate2, grove Scan, proposed map[utils2020.Coordinate2]int) bool {
	if !grove.hasElf(pos.Move(utils2020.North)) && !grove.hasElf(pos.Move(utils2020.NorthEast)) && !grove.hasElf(pos.Move(utils2020.NorthWest)) {
		grove[pos] = pos.Move(utils2020.North)
		proposed[pos.Move(utils2020.North)]++
		return true
	}
	return false
}

func moveSouth(pos utils2020.Coordinate2, grove Scan, proposed map[utils2020.Coordinate2]int) bool {
	if !grove.hasElf(pos.Move(utils2020.South)) && !grove.hasElf(pos.Move(utils2020.SouthEast)) && !grove.hasElf(pos.Move(utils2020.SouthWest)) {
		grove[pos] = pos.Move(utils2020.South)
		proposed[pos.Move(utils2020.South)]++
		return true
	}
	return false
}

func moveWest(pos utils2020.Coordinate2, grove Scan, proposed map[utils2020.Coordinate2]int) bool {
	if !grove.hasElf(pos.Move(utils2020.West)) && !grove.hasElf(pos.Move(utils2020.NorthWest)) && !grove.hasElf(pos.Move(utils2020.SouthWest)) {
		grove[pos] = pos.Move(utils2020.West)
		proposed[pos.Move(utils2020.West)]++
		return true
	}
	return false
}

func moveEast(pos utils2020.Coordinate2, grove Scan, proposed map[utils2020.Coordinate2]int) bool {
	if !grove.hasElf(pos.Move(utils2020.East)) && !grove.hasElf(pos.Move(utils2020.NorthEast)) && !grove.hasElf(pos.Move(utils2020.SouthEast)) {
		grove[pos] = pos.Move(utils2020.East)
		proposed[pos.Move(utils2020.East)]++
		return true
	}
	return false
}
