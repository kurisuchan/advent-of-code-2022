package day23_test

import (
	"testing"

	"advent-of-code-2022/pkg/day23"
)

func TestEmptyTiles(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"....#..\n..###.#\n#...#.#\n.#...##\n#.###..\n##.#.##\n.#..#..", 110},
	}

	for _, test := range tests {
		actual := day23.EmptyTiles(test.in)
		if actual != test.out {
			t.Errorf("EmptyTiles(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestSpreadOut(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"....#..\n..###.#\n#...#.#\n.#...##\n#.###..\n##.#.##\n.#..#..", 20},
	}

	for _, test := range tests {
		actual := day23.SpreadOut(test.in)
		if actual != test.out {
			t.Errorf("SpreadOut(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
