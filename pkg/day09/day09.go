package day09

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/utils"
	"strings"
)

func ShortRope(input []string) int {
	lines := utils2020.NoEmptyStrings(input)

	visited := make(map[utils2020.Coordinate2]struct{})
	var headPos, tailPos utils2020.Coordinate2

	visited[tailPos] = struct{}{}

	for i := range lines {
		parts := strings.Split(lines[i], " ")
		dir, steps := parts[0], utils2020.MustInt(parts[1])
		for s := 0; s < steps; s++ {
			switch dir {
			case "U":
				headPos = headPos.Move(utils2020.North)
			case "D":
				headPos = headPos.Move(utils2020.South)
			case "L":
				headPos = headPos.Move(utils2020.West)
			case "R":
				headPos = headPos.Move(utils2020.East)
			}

			if ((headPos.X == tailPos.X || headPos.Y == tailPos.Y) && tailPos.Distance(headPos) > 1) ||
				(headPos.X != tailPos.X && headPos.Y != tailPos.Y && tailPos.Distance(headPos) > 2) {
				tailPos = tailPos.Move(OffsetDirection(headPos, tailPos))
				visited[tailPos] = struct{}{}
			}
		}
	}
	return len(visited)
}

func LongRope(input []string) int {
	lines := utils2020.NoEmptyStrings(input)

	visited := make(map[utils2020.Coordinate2]struct{})
	positions := make([]utils2020.Coordinate2, 10)

	visited[utils2020.Coordinate2{}] = struct{}{}

	for i := range lines {
		parts := strings.Split(lines[i], " ")
		dir, steps := parts[0], utils2020.MustInt(parts[1])
		for s := 0; s < steps; s++ {
			switch dir {
			case "U":
				positions[0] = positions[0].Move(utils2020.North)
			case "D":
				positions[0] = positions[0].Move(utils2020.South)
			case "L":
				positions[0] = positions[0].Move(utils2020.West)
			case "R":
				positions[0] = positions[0].Move(utils2020.East)
			}

			for k := 1; k < len(positions); k++ {
				if ((positions[k-1].X == positions[k].X || positions[k-1].Y == positions[k].Y) && positions[k].Distance(positions[k-1]) > 1) ||
					(positions[k-1].X != positions[k].X && positions[k-1].Y != positions[k].Y && positions[k].Distance(positions[k-1]) > 2) {
					positions[k] = positions[k].Move(OffsetDirection(positions[k-1], positions[k]))
				}
			}
			visited[positions[9]] = struct{}{}
		}
	}
	return len(visited)
}

func OffsetDirection(head, tail utils2020.Coordinate2) utils2020.Coordinate2 {
	return utils2020.Coordinate2{
		X: utils.Max(-1, utils.Min(1, head.X-tail.X)),
		Y: utils.Max(-1, utils.Min(1, head.Y-tail.Y)),
	}
}
