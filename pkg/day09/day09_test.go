package day09_test

import (
	"testing"

	"advent-of-code-2022/pkg/day09"
)

func TestShortRope(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"R 4",
			"U 4",
			"L 3",
			"D 1",
			"R 4",
			"D 1",
			"L 5",
			"R 2",
		}, 13},
	}

	for _, test := range tests {
		actual := day09.ShortRope(test.in)
		if actual != test.out {
			t.Errorf("ShortRope(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestLongRope(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"R 4",
			"U 4",
			"L 3",
			"D 1",
			"R 4",
			"D 1",
			"L 5",
			"R 2",
		}, 1},
		{[]string{
			"R 5",
			"U 8",
			"L 8",
			"D 3",
			"R 17",
			"D 10",
			"L 25",
			"U 20",
		}, 36},
	}

	for _, test := range tests {
		actual := day09.LongRope(test.in)
		if actual != test.out {
			t.Errorf("LongRope(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
