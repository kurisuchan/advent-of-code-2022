package day22_test

import (
	"testing"

	"advent-of-code-2022/pkg/day22"
)

func TestMonkeyPassword(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"        ...#\n        .#..\n        #...\n        ....\n...#.......#\n........#...\n..#....#....\n..........#.\n        ...#....\n        .....#..\n        .#......\n        ......#.\n\n10R5L5R10L4R5L5", 6032},
	}

	for _, test := range tests {
		actual := day22.MonkeyPassword(test.in)
		if actual != test.out {
			t.Errorf("MonkeyPassword(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestRealInputTarget(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"        ...#\n        .#..\n        #...\n        ....\n...#.......#\n........#...\n..#....#....\n..........#.\n        ...#....\n        .....#..\n        .#......\n        ......#.\n\n10R5L5R10L4R5L5", 5031},
	}

	for _, test := range tests {
		actual := day22.CubePassword(test.in, 4, day22.TestInputTarget)
		if actual != test.out {
			t.Errorf("CubePassword(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
