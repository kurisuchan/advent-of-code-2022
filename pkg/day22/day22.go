package day22

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"regexp"
	"strings"
)

func MonkeyPassword(input string) int {
	pos, world, instructions := parseInput(input)
	orientation := 1
	for _, ins := range instructions {
		switch {
		case ins[1] != "":
			steps := utils2020.MustInt(ins[1])
			for i := 0; i < steps; i++ {
				target := pos.Move(utils2020.Coordinate2Neighbors4[orientation])
				walkable, mapped := world[target]
				if !mapped {
					// walking off edge, find other side
					newTarget := pos.Move(utils2020.Coordinate2Neighbors4[(orientation+2)%4])
					for {
						if _, mapped := world[newTarget.Move(utils2020.Coordinate2Neighbors4[(orientation+2)%4])]; mapped {
							newTarget = newTarget.Move(utils2020.Coordinate2Neighbors4[(orientation+2)%4])
						} else {
							break
						}
					}
					target = newTarget
					walkable = world[newTarget]
				}
				if !walkable {
					break
				}
				pos = target
			}
		case ins[2] != "":
			if ins[2] == "L" {
				orientation = (orientation + 3) % 4
			} else {
				orientation = (orientation + 1) % 4
			}
		}
	}

	return password(pos, orientation)
}

func CubePassword(input string, faceSize int, getNewTarget targetFunc) int {
	pos, world, instructions := parseInput(input)
	orientation := 1

	for _, ins := range instructions {
		switch {
		case ins[1] != "":
			steps := utils2020.MustInt(ins[1])
			for i := 0; i < steps; i++ {
				target := pos.Move(utils2020.Coordinate2Neighbors4[orientation])
				walkable, mapped := world[target]
				if !mapped {
					// walking off edge, find other side
					target, orientation, walkable = getNewTarget(pos, orientation, world, faceSize)
				}
				if !walkable {
					break
				}
				pos = target
			}
		case ins[2] != "":
			if ins[2] == "L" {
				orientation = (orientation + 3) % 4
			} else {
				orientation = (orientation + 1) % 4
			}
		}
	}

	return password(pos, orientation)
}

func password(pos utils2020.Coordinate2, orientation int) int {
	return 1000*(pos.Y+1) + 4*(pos.X+1) + ((orientation + 3) % 4)
}

func parseInput(input string) (utils2020.Coordinate2, map[utils2020.Coordinate2]bool, [][]string) {
	world := make(map[utils2020.Coordinate2]bool)

	var start utils2020.Coordinate2
	var haveStart bool

	for y, row := range strings.Split(input, "\n") {
		if row == "" {
			break
		}
		for x, c := range row {
			if c == ' ' {
				continue
			}
			world[utils2020.Coordinate2{X: x, Y: y}] = c == '.'
			if !haveStart && c == '.' {
				start = utils2020.Coordinate2{X: x, Y: y}
				haveStart = true
			}
		}
	}

	pattern := regexp.MustCompile(`(\d+)|([LR])`)

	return start, world, pattern.FindAllStringSubmatch(input, -1)
}

// TODO: Dynamic cube generation

var faces50 = [6]utils2020.Coordinate2{
	{50, 0},
	{100, 0},
	{50, 50},
	{0, 100},
	{50, 100},
	{0, 150},
}

var faces4 = [6]utils2020.Coordinate2{
	{8, 0},
	{0, 4},
	{4, 4},
	{8, 4},
	{8, 8},
	{12, 8},
}

type targetFunc func(utils2020.Coordinate2, int, map[utils2020.Coordinate2]bool, int) (utils2020.Coordinate2, int, bool)

func TestInputTarget(pos utils2020.Coordinate2, orientation int, world map[utils2020.Coordinate2]bool, faceSize int) (utils2020.Coordinate2, int, bool) {
	var target utils2020.Coordinate2
	var newOrientation int
	for f, face := range faces4 {
		if face.X <= pos.X && pos.X < face.X+faceSize && face.Y <= pos.Y && pos.Y < face.Y+faceSize {
			withinFace := utils2020.Coordinate2{X: pos.X - faces4[f].X, Y: pos.Y - faces4[f].Y}
			switch f {
			case 0:
				switch orientation {
				case 0:
					target.X, target.Y = faces4[1].X+faceSize-withinFace.X-1, faces4[1].Y
					newOrientation = 2
				case 1:
					target.X, target.Y = faces4[5].X+faceSize-1, faces4[5].Y+faceSize-withinFace.Y-1
					newOrientation = 3
				case 3:
					target.X, target.Y = faces4[2].X+withinFace.Y, faces4[2].Y
					newOrientation = 2
				}
			case 1:
				switch orientation {
				case 0:
					target.X, target.Y = faces4[0].X+faceSize-withinFace.X-1, faces4[0].Y
					newOrientation = 2
				case 2:
					target.X, target.Y = faces4[4].X+faceSize-withinFace.X-1, faces4[4].Y+faceSize-1
					newOrientation = 0
				case 3:
					target.X, target.Y = faces4[5].X+faceSize-withinFace.Y-1, faces4[5].Y+faceSize-1
					newOrientation = 0
				}
			case 2:
				switch orientation {
				case 0:
					target.X, target.Y = faces4[0].X, faces4[0].Y+withinFace.X
					newOrientation = 1
				case 2:
					target.X, target.Y = faces4[4].X, faces4[4].Y+faceSize-withinFace.X-1
					newOrientation = 1
				}
			case 3:
				switch orientation {
				case 1:
					target.X, target.Y = faces4[5].X+faceSize-withinFace.Y-1, faces4[5].Y
					newOrientation = 2
				}
			case 4:
				switch orientation {
				case 2:
					target.X, target.Y = faces4[1].X+faceSize-withinFace.X-1, faces4[1].Y+faceSize-1
					newOrientation = 0
				case 3:
					target.X, target.Y = faces4[2].X+faceSize-withinFace.Y-1, faces4[2].Y+faceSize-1
					newOrientation = 0
				}
			case 5:
				switch orientation {
				case 0:
					target.X, target.Y = faces4[3].X+faceSize-1, faces4[3].Y+faceSize-withinFace.X-1
					orientation = 3
				case 1:
					target.X, target.Y = faces4[0].X+faceSize-1, faces4[0].Y+faceSize-withinFace.Y-1
					newOrientation = 3
				case 2:
					target.X, target.Y = faces4[1].X, faces4[1].Y+faceSize-withinFace.X-1
					newOrientation = 1
				}
			}
			break
		}
	}

	walkable := world[target]
	if walkable {
		orientation = newOrientation
	}
	return target, orientation, walkable
}

func RealInputTarget(pos utils2020.Coordinate2, orientation int, world map[utils2020.Coordinate2]bool, faceSize int) (utils2020.Coordinate2, int, bool) {
	var target utils2020.Coordinate2
	var newOrientation int
	for f, face := range faces50 {
		if face.X <= pos.X && pos.X < face.X+faceSize && face.Y <= pos.Y && pos.Y < face.Y+faceSize {
			withinFace := utils2020.Coordinate2{X: pos.X - faces50[f].X, Y: pos.Y - faces50[f].Y}
			switch f {
			case 0:
				switch orientation {
				case 0:
					target.X, target.Y = faces50[5].X, faces50[5].Y+withinFace.X
					newOrientation = 1
				case 3:
					target.X, target.Y = faces50[3].X, faces50[3].Y+faceSize-(withinFace.Y)-1
					newOrientation = 1
				}
			case 1:
				switch orientation {
				case 0:
					target.X, target.Y = faces50[5].X+withinFace.X, faces50[5].Y+faceSize-1
					newOrientation = orientation
				case 1:
					target.X, target.Y = faces50[4].X+faceSize-1, faces50[4].Y+faceSize-withinFace.Y-1
					newOrientation = 3
				case 2:
					target.X, target.Y = faces50[2].X+faceSize-1, faces50[2].Y+withinFace.X
					newOrientation = 3
				}
			case 2:
				switch orientation {
				case 1:
					target.X, target.Y = faces50[1].X+withinFace.Y, faces50[1].Y+faceSize-1
					newOrientation = 0
				case 3:
					target.X, target.Y = faces50[3].X+withinFace.Y, faces50[3].Y
					newOrientation = 2
				}
			case 3:
				switch orientation {
				case 0:
					target.X, target.Y = faces50[2].X, faces50[2].Y+withinFace.X
					newOrientation = 1
				case 3:
					target.X, target.Y = faces50[0].X, faces50[0].Y+faceSize-withinFace.Y-1
					newOrientation = 1
				}
			case 4:
				switch orientation {
				case 1:
					target.X, target.Y = faces50[1].X+faceSize-1, faces50[1].Y+faceSize-withinFace.Y-1
					newOrientation = 3
				case 2:
					target.X, target.Y = faces50[5].X+faceSize-1, faces50[5].Y+withinFace.X
					newOrientation = 3
				}
			case 5:
				switch orientation {
				case 1:
					target.X, target.Y = faces50[4].X+withinFace.Y, faces50[4].Y+faceSize-1
					newOrientation = 0
				case 2:
					target.X, target.Y = faces50[1].X+withinFace.X, faces50[1].Y
					newOrientation = orientation
				case 3:
					target.X, target.Y = faces50[0].X+withinFace.Y, faces50[0].Y
					newOrientation = 2
				}
			}
			break
		}
	}

	walkable := world[target]
	if walkable {
		orientation = newOrientation
	}
	return target, orientation, walkable
}
