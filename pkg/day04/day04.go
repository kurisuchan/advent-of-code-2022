package day04

import (
	"advent-of-code-2020/pkg/utils"
	"regexp"
)

func Contained(input string) int {
	var count int
	pairs := parseInput(input)
	for i := range parseInput(input) {
		if pairs[i][0].Contains(pairs[i][1]) || pairs[i][1].Contains(pairs[i][0]) {
			count++
		}
	}
	return count
}

func Overlapped(input string) int {
	var count int
	pairs := parseInput(input)
	for i := range parseInput(input) {
		if pairs[i][0].Overlaps(pairs[i][1]) || pairs[i][1].Overlaps(pairs[i][0]) {
			count++
		}
	}
	return count
}

func parseInput(input string) [][2]Elf {
	var out [][2]Elf
	pattern := regexp.MustCompile(`(\d+)-(\d+),(\d+)-(\d+)`)
	matches := pattern.FindAllStringSubmatch(input, -1)
	for i := range matches {
		first := Elf{utils.MustInt(matches[i][1]), utils.MustInt(matches[i][2])}
		second := Elf{utils.MustInt(matches[i][3]), utils.MustInt(matches[i][4])}
		out = append(out, [2]Elf{first, second})
	}
	return out
}

type Elf struct {
	Min int
	Max int
}

func (e Elf) Contains(o Elf) bool {
	return e.Min <= o.Min && e.Max >= o.Max
}

func (e Elf) Overlaps(o Elf) bool {
	return (e.Min <= o.Min && e.Max >= o.Min) || (e.Min <= o.Max && e.Max >= o.Max)
}
