package day04_test

import (
	"testing"

	"advent-of-code-2022/pkg/day04"
)

func TestContained(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8", 2},
	}

	for _, test := range tests {
		actual := day04.Contained(test.in)
		if actual != test.out {
			t.Errorf("Contained(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestOverlapped(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8", 4},
	}

	for _, test := range tests {
		actual := day04.Overlapped(test.in)
		if actual != test.out {
			t.Errorf("Overlapped(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
