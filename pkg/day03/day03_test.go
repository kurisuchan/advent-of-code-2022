package day03_test

import (
	"testing"

	"advent-of-code-2022/pkg/day03"
)

func TestCompartments(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"vJrwpWtwJgWrhcsFMMfFFhFp",
			"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
			"PmmdzqPrVvPwwTWBwg",
			"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
			"ttgJtRGJQctTZtZT",
			"CrZsJsPPZsGzwwsLwLmpwMDw",
		}, 157},
	}

	for _, test := range tests {
		actual := day03.Compartments(test.in)
		if actual != test.out {
			t.Errorf("Compartments(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestBadges(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"vJrwpWtwJgWrhcsFMMfFFhFp",
			"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
			"PmmdzqPrVvPwwTWBwg",
			"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
			"ttgJtRGJQctTZtZT",
			"CrZsJsPPZsGzwwsLwLmpwMDw",
		}, 70},
	}

	for _, test := range tests {
		actual := day03.Badges(test.in)
		if actual != test.out {
			t.Errorf("Badges(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
