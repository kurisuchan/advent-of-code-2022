package day03

func Compartments(input []string) int {
	var sum int
outer:
	for _, rucksack := range input {
		half := len(rucksack) / 2
		for l := 0; l < half; l++ {
			for r := half; r < len(rucksack); r++ {
				if rucksack[l] == rucksack[r] {
					sum += priority(rucksack[l])
					continue outer
				}
			}
		}
	}
	return sum
}

func Badges(input []string) int {
	var sum int
outer:
	for i := 0; i < len(input); i += 3 {
		// brute force baby
		for a := range input[i] {
			for b := range input[i+1] {
				if input[i][a] != input[i+1][b] {
					continue
				}
				for c := range input[i+2] {
					if input[i+1][b] != input[i+2][c] {
						continue
					}
					if input[i][a] == input[i+1][b] && input[i][a] == input[i+2][c] {
						sum += priority(input[i][a])
						continue outer
					}
				}
			}
		}
	}
	return sum
}

func priority(b byte) int {
	if b < 'a' {
		return int(b - 'A' + 27)
	}
	return int(b - 'a' + 1)
}
