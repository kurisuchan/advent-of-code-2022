package day08

import (
	"advent-of-code-2020/pkg/utils"
	"strings"
)

func VisibleTrees(input string) int {
	g := parseInput(input)
	var count int

	for pos := range g.trees {
		if g.Visible(pos) {
			count++
		}
	}

	return count
}

func ScenicView(input string) int {
	g := parseInput(input)

	var best int
	for pos := range g.trees {
		score := g.ViewingDistance(pos)
		if score > best {
			best = score
		}
	}

	return best
}

type Map struct {
	trees map[utils.Coordinate2]int32
	size  utils.Coordinate2
}

func parseInput(input string) Map {
	var m Map
	m.trees = make(map[utils.Coordinate2]int32)
	for y, line := range strings.Split(input, "\n") {
		for x, v := range line {
			m.trees[utils.Coordinate2{X: x, Y: y}] = v - '0'
			if x > m.size.X {
				m.size.X = x
			}
			if y > m.size.Y {
				m.size.Y = y
			}
		}
	}
	return m
}

func (m Map) IsEdge(pos utils.Coordinate2) bool {
	return pos.X == 0 || pos.Y == 0 || pos.X == m.size.X || pos.Y == m.size.Y
}

func (m Map) Visible(pos utils.Coordinate2) bool {
	if m.IsEdge(pos) {
		return true
	}

	for i := range utils.Coordinate2Neighbors4 {
		if !m.Obstructed(pos, utils.Coordinate2Neighbors4[i], m.trees[pos]) {
			return true
		}
	}
	return false
}

func (m Map) Obstructed(pos utils.Coordinate2, dir utils.Coordinate2, limit int32) bool {
	if m.IsEdge(pos) {
		return false
	}

	c := pos.Move(dir)
	if m.trees[c] >= limit {
		return true
	}
	return m.Obstructed(c, dir, limit)
}

func (m Map) ViewingDistance(pos utils.Coordinate2) int {
	distance := 1

	for i := range utils.Coordinate2Neighbors4 {
		var seen int

		c := pos.Move(utils.Coordinate2Neighbors4[i])
		for c.X >= 0 && c.Y >= 0 && c.X <= m.size.X && c.Y <= m.size.Y {
			seen++
			if m.trees[c] >= m.trees[pos] {
				break
			}
			c = c.Move(utils.Coordinate2Neighbors4[i])
		}
		distance *= seen
		if distance == 0 {
			return 0
		}
	}
	return distance
}
