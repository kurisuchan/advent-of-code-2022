package day08_test

import (
	"testing"

	"advent-of-code-2022/pkg/day08"
)

func TestVisibleTrees(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"30373\n25512\n65332\n33549\n35390", 21},
	}

	for _, test := range tests {
		actual := day08.VisibleTrees(test.in)
		if actual != test.out {
			t.Errorf("VisibleTrees(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestScenicView(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"30373\n25512\n65332\n33549\n35390", 8},
	}

	for _, test := range tests {
		actual := day08.ScenicView(test.in)
		if actual != test.out {
			t.Errorf("ScenicView(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
