package day01_test

import (
	"testing"

	"advent-of-code-2022/pkg/day01"
)

func TestCalories(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000", 24000},
	}

	for _, test := range tests {
		actual := day01.Calories(test.in)
		if actual != test.out {
			t.Errorf("Calories(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestTopCalories(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000", 45000},
	}

	for _, test := range tests {
		actual := day01.TopCalories(test.in)
		if actual != test.out {
			t.Errorf("TopCalories(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
