package day01

import (
	"advent-of-code-2020/pkg/utils"
	"sort"
	"strings"
)

func Calories(input string) int {
	var max int
	for _, food := range strings.Split(input, "\n\n") {
		s := getCalories(food)
		if s > max {
			max = s
		}
	}
	return max
}

func TopCalories(input string) int {
	var calories []int
	for _, food := range strings.Split(input, "\n\n") {
		calories = append(calories, getCalories(food))
	}
	sort.Sort(sort.Reverse(sort.IntSlice(calories)))
	return calories[0] + calories[1] + calories[2]
}

func getCalories(input string) int {
	var s int
	for _, calories := range strings.Split(input, "\n") {
		if calories != "" {
			s += utils.MustInt(calories)
		}
	}
	return s
}
