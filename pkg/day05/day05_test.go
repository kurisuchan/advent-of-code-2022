package day05_test

import (
	"testing"

	"advent-of-code-2022/pkg/day05"
)

func TestCrateMover9000(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2", "CMZ"},
	}

	for _, test := range tests {
		actual := day05.CrateMover9000(test.in)
		if actual != test.out {
			t.Errorf("CrateMover9000(%q) => %s, want %s", test.in, actual, test.out)
		}
	}
}

func TestCrateMover9001(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2", "MCD"},
	}

	for _, test := range tests {
		actual := day05.CrateMover9001(test.in)
		if actual != test.out {
			t.Errorf("CrateMover9001(%q) => %s, want %s", test.in, actual, test.out)
		}
	}
}
