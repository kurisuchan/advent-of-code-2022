package day05

import (
	"advent-of-code-2020/pkg/utils"
	"regexp"
	"strings"
)

func CrateMover9000(input string) string {
	hold, moves := parseInput(input)

	for i := range moves {
		hold.Move(moves[i][2][0], moves[i][3][0], utils.MustInt(moves[i][1]))
	}

	return hold.TopString()
}

func CrateMover9001(input string) string {
	hold, moves := parseInput(input)

	for i := range moves {
		hold.MoveN(moves[i][2][0], moves[i][3][0], utils.MustInt(moves[i][1]))
	}

	return hold.TopString()
}

type CargoHold struct {
	stacks map[byte][]byte
	key    []byte
}

func parseInput(input string) (CargoHold, [][]string) {
	parts := strings.Split(input, "\n\n")
	arrangement := strings.Split(parts[0], "\n")

	bottom := arrangement[len(arrangement)-1]

	stacks := make(map[byte][]byte, len(bottom)/4)
	var ids []byte

	for i := 1; i < len(bottom); i += 4 {
		stacks[bottom[i]] = []byte{}
		ids = append(ids, bottom[i])
	}

	for l := len(arrangement) - 2; l >= 0; l-- {
		for i := 1; i < len(arrangement[l]); i += 4 {
			pos := ids[(i-1)/4]
			crate := arrangement[l][i]
			if crate != ' ' {
				stacks[pos] = append(stacks[pos], crate)
			}
		}
	}

	ins := regexp.MustCompile(`move (\d+) from (\d) to (\d)`)
	matches := ins.FindAllStringSubmatch(parts[1], -1)

	return CargoHold{stacks, ids}, matches
}

func (c *CargoHold) Move(source, target byte, amount int) {
	for i := 0; i < amount; i++ {
		c.Push(target, c.Pop(source))
	}
}

func (c *CargoHold) MoveN(source, target byte, amount int) {
	c.PushN(target, c.PopN(source, amount))
}

func (c *CargoHold) Pop(key byte) byte {
	n := len(c.stacks[key]) - 1
	o := c.stacks[key][n]
	c.stacks[key] = c.stacks[key][:n]
	return o
}

func (c *CargoHold) PopN(key byte, amount int) []byte {
	n := len(c.stacks[key]) - amount
	o := c.stacks[key][n:]
	c.stacks[key] = c.stacks[key][:n]
	return o
}

func (c *CargoHold) Push(key, value byte) {
	c.stacks[key] = append(c.stacks[key], value)
}

func (c *CargoHold) PushN(key byte, value []byte) {
	c.stacks[key] = append(c.stacks[key], value...)
}

func (c CargoHold) TopString() string {
	var out strings.Builder
	for i := range c.key {
		out.WriteByte(c.Pop(c.key[i]))
	}
	return out.String()
}
