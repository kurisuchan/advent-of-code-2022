package day19_test

import (
	"testing"

	"advent-of-code-2022/pkg/day19"
)

func TestBlueprintQuality(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.\nBlueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.\n", 33},
	}

	for _, test := range tests {
		actual := day19.BlueprintQuality(test.in)
		if actual != test.out {
			t.Errorf("BlueprintQuality(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestHungryElephants(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.\nBlueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.\n", 56 * 62},
	}

	for _, test := range tests {
		actual := day19.HungryElephants(test.in)
		if actual != test.out {
			t.Errorf("HungryElephants(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
