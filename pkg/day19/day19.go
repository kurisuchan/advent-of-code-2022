package day19

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/utils"
	"regexp"
)

func BlueprintQuality(input string) int {
	blueprints := parseInput(input)
	var result int

	for bp := range blueprints {
		r := startFactory(blueprints[bp], 24)
		result += r * (bp + 1)
	}

	return result
}

func HungryElephants(input string) int {
	blueprints := parseInput(input)
	blueprints = blueprints[:utils.Min(3, len(blueprints))]
	var result int = 1

	for bp := range blueprints {
		r := startFactory(blueprints[bp], 32)
		result *= r
	}

	return result
}

func startFactory(blueprint Blueprint, time int) int {
	return factory(blueprint, Stuff{}, Stuff{Ore: 1}, time, All)
}

func factory(blueprint Blueprint, materials Stuff, robots Stuff, time int, allowedToBuild Robot) int {
	if time == 0 {
		return materials.Geode
	}

	time -= 1
	var best int
	affordAndWant := blueprint.affordAndWantMore(materials, robots) & allowedToBuild

	if affordAndWant.has(Geode) && time > 0 {
		result := factory(blueprint, materials.deduct(blueprint.GeodeRobot).generate(robots), robots.add(0, 0, 0, 1), time, All)
		if result > best {
			best = result
		}
	} else {
		if (affordAndWant & allowedToBuild).has(Obsidian) && time > 1 {
			result := factory(blueprint, materials.deduct(blueprint.ObsidianRobot).generate(robots), robots.add(0, 0, 1, 0), time, All)
			if result > best {
				best = result
			}
		}

		if (affordAndWant & allowedToBuild).has(Clay) && time > 2 {
			result := factory(blueprint, materials.deduct(blueprint.ClayRobot).generate(robots), robots.add(0, 1, 0, 0), time, All)
			if result > best {
				best = result
			}
		}

		if (affordAndWant & allowedToBuild).has(Ore) && time > 1 {
			result := factory(blueprint, materials.deduct(blueprint.OreRobot).generate(robots), robots.add(1, 0, 0, 0), time, All)
			if result > best {
				best = result
			}
		}

		// if we could have built a robot, but chose not to, we probably don't want to build one next time either
		result := factory(blueprint, materials.generate(robots), robots, time, allowedToBuild&^affordAndWant)
		if result > best {
			best = result
		}
	}

	return best
}

func parseInput(input string) []Blueprint {
	pattern := regexp.MustCompile(`ore robot costs (\d+) ore.*clay robot costs (\d+) ore.*obsidian robot costs (\d+) ore and (\d+) clay.*geode robot costs (\d+) ore and (\d+) obsidian`)
	matches := pattern.FindAllStringSubmatch(input, -1)
	var blueprints []Blueprint
	for m := range matches {
		bp := Blueprint{
			OreRobot:      Stuff{Ore: utils2020.MustInt(matches[m][1])},
			ClayRobot:     Stuff{Ore: utils2020.MustInt(matches[m][2])},
			ObsidianRobot: Stuff{Ore: utils2020.MustInt(matches[m][3]), Clay: utils2020.MustInt(matches[m][4])},
			GeodeRobot:    Stuff{Ore: utils2020.MustInt(matches[m][5]), Obsidian: utils2020.MustInt(matches[m][6])},
		}
		blueprints = append(blueprints, bp)
	}
	return blueprints
}

type Robot byte

const (
	Ore Robot = 1 << iota
	Clay
	Obsidian
	Geode
	All = Ore | Clay | Obsidian | Geode
)

func (r Robot) has(robot Robot) bool {
	return r&robot != 0
}

type Stuff struct {
	Ore      int
	Clay     int
	Obsidian int
	Geode    int
}

func (m Stuff) generate(robots Stuff) Stuff {
	return m.add(robots.Ore, robots.Clay, robots.Obsidian, robots.Geode)
}

func (m Stuff) deduct(materials Stuff) Stuff {
	return m.add(-materials.Ore, -materials.Clay, -materials.Obsidian, -materials.Geode)
}

func (m Stuff) add(ore, clay, obsidian, geode int) Stuff {
	m.Ore += ore
	m.Clay += clay
	m.Obsidian += obsidian
	m.Geode += geode
	return m
}

type Blueprint struct {
	OreRobot      Stuff
	ClayRobot     Stuff
	ObsidianRobot Stuff
	GeodeRobot    Stuff
}

func (bp Blueprint) affordAndWantMore(materials Stuff, robots Stuff) Robot {
	var afford Robot
	if materials.Ore >= bp.GeodeRobot.Ore && materials.Obsidian >= bp.GeodeRobot.Obsidian {
		afford |= Geode
	}
	if materials.Ore >= bp.ObsidianRobot.Ore && materials.Clay >= bp.ObsidianRobot.Clay && robots.Obsidian < bp.GeodeRobot.Obsidian {
		afford |= Obsidian
	}
	if materials.Ore >= bp.ClayRobot.Ore && robots.Clay < bp.ObsidianRobot.Clay {
		afford |= Clay
	}
	if materials.Ore >= bp.OreRobot.Ore && (robots.Ore < bp.GeodeRobot.Ore || robots.Ore < bp.ClayRobot.Ore || robots.Ore < bp.ObsidianRobot.Ore || robots.Ore < bp.OreRobot.Ore) {
		afford |= Ore
	}
	return afford
}
