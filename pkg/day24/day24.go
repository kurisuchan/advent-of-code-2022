package day24

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day12"
	"advent-of-code-2022/pkg/utils"
	"strings"
)

var world World

func BlizzardValley(input string) int {
	world = parseInput(input)

	_, distance, _ := day12.Path(Coordinate3{X: world.start.X, Y: world.start.Y, Z: 0}, Coordinate3{X: world.goal.X, Y: world.goal.Y})

	return distance
}

func SnackTrack(input string) int {
	world = parseInput(input)

	var totalTime int
	_, distance, _ := day12.Path(Coordinate3{X: world.start.X, Y: world.start.Y, Z: 0}, Coordinate3{X: world.goal.X, Y: world.goal.Y})
	totalTime += distance
	_, distance, _ = day12.Path(Coordinate3{X: world.goal.X, Y: world.goal.Y, Z: totalTime}, Coordinate3{X: world.start.X, Y: world.start.Y})
	totalTime += distance
	_, distance, _ = day12.Path(Coordinate3{X: world.start.X, Y: world.start.Y, Z: totalTime}, Coordinate3{X: world.goal.X, Y: world.goal.Y})

	return totalTime + distance
}

type Blizzard struct {
	pos       utils2020.Coordinate2
	direction int
}

func parseInput(input string) World {
	var blizzards []Blizzard
	rows := strings.Split(strings.TrimSpace(input), "\n")
	dims := Coordinate3{
		X: len(rows[0]) - 2,
		Y: len(rows) - 2,
		Z: utils.LeastCommonMultiple(len(rows[0])-2, len(rows)-2),
	}
	var start, goal utils2020.Coordinate2
	for x := 1; x < len(rows[0])-1; x++ {
		if rows[0][x] == '.' {
			start.X = x - 1
			start.Y = -1
			break
		}
	}
	for x := 1; x < len(rows[len(rows)-1])-1; x++ {
		if rows[len(rows)-1][x] == '.' {
			goal.X = x - 1
			goal.Y = len(rows) - 2
			break
		}
	}
	fullBlizzard := make(map[Coordinate3]struct{})
	for y := 1; y < len(rows)-1; y++ {
		for x := 1; x < len(rows[y])-1; x++ {
			pos := utils2020.Coordinate2{X: x - 1, Y: y - 1}
			pos3 := Coordinate3{X: x - 1, Y: y - 1, Z: 0}
			switch rows[y][x] {
			case '^':
				blizzards = append(blizzards, Blizzard{pos: pos, direction: 0})
				fullBlizzard[pos3] = struct{}{}
			case '>':
				blizzards = append(blizzards, Blizzard{pos: pos, direction: 1})
				fullBlizzard[pos3] = struct{}{}
			case 'v':
				blizzards = append(blizzards, Blizzard{pos: pos, direction: 2})
				fullBlizzard[pos3] = struct{}{}
			case '<':
				blizzards = append(blizzards, Blizzard{pos: pos, direction: 3})
				fullBlizzard[pos3] = struct{}{}
			}
		}
	}

	for z := 0; z < dims.Z; z++ {
		var nextBlizzards []Blizzard
		for _, blizzard := range blizzards {
			fullBlizzard[Coordinate3{X: blizzard.pos.X, Y: blizzard.pos.Y, Z: z}] = struct{}{}

			blizzard.pos = blizzard.pos.Move(utils2020.Coordinate2Neighbors4[blizzard.direction])
			blizzard.pos.X = blizzard.pos.X % dims.X
			if blizzard.pos.X < 0 {
				blizzard.pos.X += dims.X
			}
			if blizzard.pos.Y < 0 {
				blizzard.pos.Y += dims.Y
			}
			blizzard.pos.Y = blizzard.pos.Y % dims.Y
			nextBlizzards = append(nextBlizzards, blizzard)
		}
		blizzards = nextBlizzards
	}

	return World{
		field: fullBlizzard,
		dims:  dims,
		start: start,
		goal:  goal,
	}
}

type World struct {
	field map[Coordinate3]struct{}
	dims  Coordinate3
	start utils2020.Coordinate2
	goal  utils2020.Coordinate2
}

func (w World) traversable(pos Coordinate3) bool {
	_, found := w.field[Coordinate3{X: pos.X, Y: pos.Y, Z: pos.Z % w.dims.Z}]
	return !found
}

func (w World) inBounds(pos Coordinate3) bool {
	return ((pos.is(w.start) || pos.is(w.goal)) || (pos.X >= 0 && pos.Y >= 0 && pos.X < w.dims.X && pos.Y < w.dims.Y)) && pos.Z <= w.dims.X*w.dims.Y*w.dims.Z
}

func (w World) move(pos Coordinate3, distance utils2020.Coordinate2) Coordinate3 {
	return Coordinate3{
		X: pos.X + distance.X,
		Y: pos.Y + distance.Y,
		Z: pos.Z + 1,
	}
}

type Coordinate3 struct {
	X int
	Y int
	Z int
}

func (c Coordinate3) is(o utils2020.Coordinate2) bool {
	return c.X == o.X && c.Y == o.Y
}

func (c Coordinate3) Neighbors() []day12.Traversable {
	var out []day12.Traversable

	target := Coordinate3{X: c.X, Y: c.Y, Z: c.Z + 1}
	if world.traversable(target) && world.inBounds(target) {
		out = append(out, target)
	}
	for i := range utils2020.Coordinate2Neighbors4 {
		target = world.move(c, utils2020.Coordinate2Neighbors4[i])
		if world.traversable(target) && world.inBounds(target) {
			out = append(out, target)
		}
	}

	return out
}

func (c Coordinate3) NeighborCost(to day12.Traversable) int {
	return 1
}

func (c Coordinate3) EstimateCost(to day12.Traversable) int {
	return c.Distance(to.(Coordinate3))
}

func (c Coordinate3) Equal(to day12.Traversable) bool {
	return c.X == to.(Coordinate3).X && c.Y == to.(Coordinate3).Y
}

func (c Coordinate3) Distance(to Coordinate3) int {
	return utils2020.AbsInt(c.X-to.X) + utils2020.AbsInt(c.Y-to.Y)
}
