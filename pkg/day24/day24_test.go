package day24_test

import (
	"testing"

	"advent-of-code-2022/pkg/day24"
)

func TestBlizzardValley(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"#.######\n#>>.<^<#\n#.<..<<#\n#>v.><>#\n#<^v^^>#\n######.#", 18},
	}

	for _, test := range tests {
		actual := day24.BlizzardValley(test.in)
		if actual != test.out {
			t.Errorf("BlizzardValley(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestSnackTrack(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"#.######\n#>>.<^<#\n#.<..<<#\n#>v.><>#\n#<^v^^>#\n######.#", 54},
	}

	for _, test := range tests {
		actual := day24.SnackTrack(test.in)
		if actual != test.out {
			t.Errorf("SnackTrack(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
