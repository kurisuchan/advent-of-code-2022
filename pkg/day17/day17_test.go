package day17_test

import (
	"testing"

	"advent-of-code-2022/pkg/day17"
)

func TestTowerOfRocks(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>\n", 3068},
	}

	for _, test := range tests {
		actual := day17.TowerOfRocks(test.in)
		if actual != test.out {
			t.Errorf("TowerOfRocks(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestManyRocks(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>\n", 1514285714288},
	}

	for _, test := range tests {
		actual := day17.ManyRocks(test.in)
		if actual != test.out {
			t.Errorf("ManyRocks(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
