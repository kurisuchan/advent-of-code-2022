package day17

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"strings"
)

const limit = 1000000000000

func TowerOfRocks(input string) int {
	wind := strings.TrimSpace(input)

	chamber := make(map[utils2020.Coordinate2]struct{})
	var highestPoint, windIndex int

	for rockNum := 0; rockNum < 2022; rockNum++ {
		rock := Rocks[rockNum%len(Rocks)]
		windIndex, highestPoint = addRock(chamber, wind, rock, windIndex, highestPoint)
	}

	return highestPoint
}

func ManyRocks(input string) int {
	wind := strings.TrimSpace(input)

	chamber := make(map[utils2020.Coordinate2]struct{})
	seen := make(map[cacheKey]cacheValue)
	var highestPoint, windIndex int

	for rockNum := 0; rockNum < limit; rockNum++ {
		key := cacheKey{rockIndex: rockNum % len(Rocks), windIndex: windIndex % len(wind)}

		if cached, ok := seen[key]; ok {
			if (limit-rockNum)%(rockNum-cached.rockNum) == 0 {
				return highestPoint + (limit-rockNum)/(rockNum-cached.rockNum)*(highestPoint-cached.highestPoint)
			}
		}
		seen[key] = cacheValue{rockNum: rockNum, highestPoint: highestPoint}

		rock := Rocks[rockNum%len(Rocks)]
		windIndex, highestPoint = addRock(chamber, wind, rock, windIndex, highestPoint)
	}

	return highestPoint
}

type cacheKey struct {
	rockIndex int
	windIndex int
}

type cacheValue struct {
	rockNum      int
	highestPoint int
}

func addRock(chamber map[utils2020.Coordinate2]struct{}, wind string, rock Rock, windIndex, highestPoint int) (int, int) {
	bottomLeft := utils2020.Coordinate2{X: 2, Y: highestPoint + 3}

	for {
		var offset utils2020.Coordinate2
		if wind[windIndex%len(wind)] == '<' && bottomLeft.X > 0 {
			offset.X = -1
		} else if wind[windIndex%len(wind)] == '>' && bottomLeft.X+rock.size.X < 7 {
			offset.X = 1
		}

		if offset.X != 0 {
			for pos := range rock.pattern {
				if _, found := chamber[bottomLeft.Move(pos).Move(offset)]; found {
					offset.X = 0
					break
				}
			}
		}

		windIndex++

		if bottomLeft.Y > 0 {
			offset.Y = -1
			for pos := range rock.pattern {
				if _, found := chamber[bottomLeft.Move(pos).Move(offset)]; found {
					offset.Y = 0
					break
				}
			}
		}

		if offset.Y == 0 {
			for pos := range rock.pattern {
				chamber[bottomLeft.Move(pos).Move(offset)] = struct{}{}
			}
			highPoint := bottomLeft.Move(offset).Y + rock.size.Y
			if highPoint > highestPoint {
				highestPoint = highPoint
			}

			return windIndex, highestPoint
		}

		bottomLeft = bottomLeft.Move(offset)
	}
}

type Rock struct {
	pattern map[utils2020.Coordinate2]struct{}
	size    utils2020.Coordinate2
}

var Rocks = []Rock{
	{
		pattern: map[utils2020.Coordinate2]struct{}{
			utils2020.Coordinate2{X: 0, Y: 0}: {},
			utils2020.Coordinate2{X: 1, Y: 0}: {},
			utils2020.Coordinate2{X: 2, Y: 0}: {},
			utils2020.Coordinate2{X: 3, Y: 0}: {},
		},
		size: utils2020.Coordinate2{X: 4, Y: 1},
	},
	{
		pattern: map[utils2020.Coordinate2]struct{}{
			utils2020.Coordinate2{X: 1, Y: 2}: {},
			utils2020.Coordinate2{X: 0, Y: 1}: {},
			utils2020.Coordinate2{X: 1, Y: 1}: {},
			utils2020.Coordinate2{X: 2, Y: 1}: {},
			utils2020.Coordinate2{X: 1, Y: 0}: {},
		},
		size: utils2020.Coordinate2{X: 3, Y: 3},
	},
	{
		pattern: map[utils2020.Coordinate2]struct{}{
			utils2020.Coordinate2{X: 2, Y: 2}: {},
			utils2020.Coordinate2{X: 2, Y: 1}: {},
			utils2020.Coordinate2{X: 0, Y: 0}: {},
			utils2020.Coordinate2{X: 1, Y: 0}: {},
			utils2020.Coordinate2{X: 2, Y: 0}: {},
		},
		size: utils2020.Coordinate2{X: 3, Y: 3},
	},
	{
		pattern: map[utils2020.Coordinate2]struct{}{
			utils2020.Coordinate2{X: 0, Y: 3}: {},
			utils2020.Coordinate2{X: 0, Y: 2}: {},
			utils2020.Coordinate2{X: 0, Y: 1}: {},
			utils2020.Coordinate2{X: 0, Y: 0}: {},
		},
		size: utils2020.Coordinate2{X: 1, Y: 4},
	},
	{
		pattern: map[utils2020.Coordinate2]struct{}{
			utils2020.Coordinate2{X: 0, Y: 1}: {},
			utils2020.Coordinate2{X: 1, Y: 1}: {},
			utils2020.Coordinate2{X: 0, Y: 0}: {},
			utils2020.Coordinate2{X: 1, Y: 0}: {},
		},
		size: utils2020.Coordinate2{X: 2, Y: 2},
	},
}
