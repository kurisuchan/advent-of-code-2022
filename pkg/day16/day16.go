package day16

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/day12"
	"golang.org/x/exp/maps"
	"golang.org/x/exp/slices"
	"regexp"
	"strings"
)

const (
	startValve    = "AA"
	soloLimit     = 30
	elephantLimit = 26
)

func PressureRelease(input string) int {
	valves, distances, todo := parseInput(input)

	var best int
	combinations := allCombinations(valves, distances, startValve, todo, nil, soloLimit)
	for i := range combinations {
		result := OpenValves(valves, distances, startValve, combinations[i], soloLimit)
		if result > best {
			best = result
		}
	}
	return best
}

func ElephantRelease(input string) int {
	valves, distances, todo := parseInput(input)

	combinations := allCombinations(valves, distances, startValve, todo, nil, elephantLimit)
	combinationRelease := make(map[uint64]int)
	for i := range combinations {
		release := OpenValves(valves, distances, startValve, combinations[i], elephantLimit)

		var mask uint64
		for valve := range combinations[i] {
			mask |= valves[combinations[i][valve]].key
		}

		if release > combinationRelease[mask] {
			combinationRelease[mask] = release
		}
	}

	var bestCombination int
	for a := range combinationRelease {
		for b := range combinationRelease {
			if a&b != 0 {
				continue
			}
			combination := combinationRelease[a] + combinationRelease[b]
			if combination > bestCombination {
				bestCombination = combination
			}
		}
	}

	return bestCombination
}

func allCombinations(valves map[string]*Valve, distances map[string]map[string]int, node string, todo map[string]struct{}, done []string, time int) [][]string {
	var out [][]string
	for next := range todo {
		newTodo := maps.Clone(todo)
		newDone := slices.Clone(done)

		delete(newTodo, next)
		cost := distances[node][next] + 1

		if cost < time {
			out = append(out, allCombinations(valves, distances, next, newTodo, append(newDone, next), time-cost)...)
		}
	}

	return append(out, done)
}

func OpenValves(valves map[string]*Valve, distances map[string]map[string]int, current string, todo []string, time int) int {
	var pressure int
	for _, next := range todo {
		time -= distances[current][next] + 1
		pressure += time * valves[next].flowRate
		current = next
	}
	return pressure
}

func parseInput(input string) (map[string]*Valve, map[string]map[string]int, map[string]struct{}) {
	pattern := regexp.MustCompile(`Valve (\w+) has flow rate=(\d+); tunnels? leads? to valves? ([\w, ]+)`)
	valves := make(map[string]*Valve)

	for i, m := range pattern.FindAllStringSubmatch(input, -1) {
		valve := m[1]
		flowRate := utils2020.MustInt(m[2])
		connections := strings.Split(m[3], ", ")

		valves[valve] = &Valve{
			flowRate: flowRate,
			tunnels:  connections,
			world:    valves,
			key:      1 << i,
		}
	}

	distances := make(map[string]map[string]int)
	withFlows := make(map[string]struct{})

	for valve := range valves {
		if valves[valve].flowRate > 0 && valve != startValve {
			withFlows[valve] = struct{}{}
		}

		distances[valve] = make(map[string]int)
		for other := range valves {
			if valve == other {
				continue
			}
			distance := valves[valve].ShortestPath(valves[other])
			if distance > 0 {
				distances[valve][other] = distance
			}
		}
	}

	return valves, distances, withFlows
}

type Valve struct {
	world    map[string]*Valve
	flowRate int
	tunnels  []string
	key      uint64
}

func (v *Valve) Neighbors() []day12.Traversable {
	var out []day12.Traversable
	for i := range v.tunnels {
		out = append(out, v.world[v.tunnels[i]])
	}
	return out
}

func (v *Valve) NeighborCost(_ day12.Traversable) int {
	return 1
}

func (v *Valve) EstimateCost(_ day12.Traversable) int {
	return 1
}

func (v *Valve) Equal(to day12.Traversable) bool {
	return v == to.(*Valve)
}

func (v *Valve) ShortestPath(target *Valve) int {
	_, distance, _ := day12.Path(v, target)
	return distance
}
