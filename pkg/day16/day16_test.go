package day16_test

import (
	"testing"

	"advent-of-code-2022/pkg/day16"
)

func TestPressureRelease(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Valve AA has flow rate=0; tunnels lead to valves DD, II, BB\nValve BB has flow rate=13; tunnels lead to valves CC, AA\nValve CC has flow rate=2; tunnels lead to valves DD, BB\nValve DD has flow rate=20; tunnels lead to valves CC, AA, EE\nValve EE has flow rate=3; tunnels lead to valves FF, DD\nValve FF has flow rate=0; tunnels lead to valves EE, GG\nValve GG has flow rate=0; tunnels lead to valves FF, HH\nValve HH has flow rate=22; tunnel leads to valve GG\nValve II has flow rate=0; tunnels lead to valves AA, JJ\nValve JJ has flow rate=21; tunnel leads to valve II", 1651},
	}

	for _, test := range tests {
		actual := day16.PressureRelease(test.in)
		if actual != test.out {
			t.Errorf("PressureRelease(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestElephantRelease(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"Valve AA has flow rate=0; tunnels lead to valves DD, II, BB\nValve BB has flow rate=13; tunnels lead to valves CC, AA\nValve CC has flow rate=2; tunnels lead to valves DD, BB\nValve DD has flow rate=20; tunnels lead to valves CC, AA, EE\nValve EE has flow rate=3; tunnels lead to valves FF, DD\nValve FF has flow rate=0; tunnels lead to valves EE, GG\nValve GG has flow rate=0; tunnels lead to valves FF, HH\nValve HH has flow rate=22; tunnel leads to valve GG\nValve II has flow rate=0; tunnels lead to valves AA, JJ\nValve JJ has flow rate=21; tunnel leads to valve II", 1707},
	}

	for _, test := range tests {
		actual := day16.ElephantRelease(test.in)
		if actual != test.out {
			t.Errorf("ElephantRelease(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
