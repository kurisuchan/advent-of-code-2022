package day25_test

import (
	"testing"

	"advent-of-code-2022/pkg/day25"
)

func TestBob(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"1=-0-2\n12111\n2=0=\n21\n2=01\n111\n20012\n112\n1=-1=\n1-12\n12\n1=\n122\n", "2=-1=0"},
	}

	for _, test := range tests {
		actual := day25.Bob(test.in)
		if actual != test.out {
			t.Errorf("Bob(%q) => %s, want %s", test.in, actual, test.out)
		}
	}
}

func TestToSnafu(t *testing.T) {
	tests := []struct {
		in  int
		out string
	}{
		{4890, "2=-1=0"},
		{1, "1"},
		{2, "2"},
		{3, "1="},
		{4, "1-"},
		{5, "10"},
		{6, "11"},
		{7, "12"},
		{8, "2="},
		{9, "2-"},
		{10, "20"},
		{15, "1=0"},
		{20, "1-0"},
		{2022, "1=11-2"},
		{12345, "1-0---0"},
		{314159265, "1121-1110-1=0"},
		{1747, "1=-0-2"},
		{906, "12111"},
		{198, "2=0="},
		{11, "21"},
		{201, "2=01"},
		{31, "111"},
		{1257, "20012"},
		{32, "112"},
		{353, "1=-1="},
		{107, "1-12"},
		{7, "12"},
		{3, "1="},
		{37, "122"},
	}

	for _, test := range tests {
		actual := day25.ToSnafu(test.in)
		if actual != test.out {
			t.Errorf("ToSnafu(%d) => %s, want %s", test.in, actual, test.out)
		}
	}
}

func TestToInt(t *testing.T) {
	tests := []struct {
		out int
		in  string
	}{
		{4890, "2=-1=0"},
		{1, "1"},
		{2, "2"},
		{3, "1="},
		{4, "1-"},
		{5, "10"},
		{6, "11"},
		{7, "12"},
		{8, "2="},
		{9, "2-"},
		{10, "20"},
		{15, "1=0"},
		{20, "1-0"},
		{2022, "1=11-2"},
		{12345, "1-0---0"},
		{314159265, "1121-1110-1=0"},
		{1747, "1=-0-2"},
		{906, "12111"},
		{198, "2=0="},
		{11, "21"},
		{201, "2=01"},
		{31, "111"},
		{1257, "20012"},
		{32, "112"},
		{353, "1=-1="},
		{107, "1-12"},
		{7, "12"},
		{3, "1="},
		{37, "122"},
	}

	for _, test := range tests {
		actual := day25.ToInt(test.in)
		if actual != test.out {
			t.Errorf("ToInt(%s) => %d, want %d", test.in, actual, test.out)
		}
	}
}
