package day25

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"math"
	"strconv"
	"strings"
)

func Bob(input string) string {
	var sum int
	for _, number := range utils2020.NoEmptyStrings(strings.Split(input, "\n")) {
		sum += ToInt(number)
	}
	return ToSnafu(sum)
}

func ToInt(snafu string) int {
	var result int
	for x := len(snafu) - 1; x >= 0; x-- {
		result += int(math.Pow(5, float64(len(snafu)-x-1))) * getDigit(snafu[x])
	}
	return result
}

func getDigit(c byte) int {
	switch c {
	case '-':
		return -1
	case '=':
		return -2
	default:
		return int(c - '0')
	}
}

func ToSnafu(number int) string {
	var snafu string
	if number > 0 {
		full, remainder := number/5, number%5
		switch remainder {
		case 3:
			snafu = ToSnafu(full+1) + "="
		case 4:
			snafu = ToSnafu(full+1) + "-"
		default:
			snafu = ToSnafu(full) + strconv.Itoa(remainder)
		}
	}
	return snafu
}
