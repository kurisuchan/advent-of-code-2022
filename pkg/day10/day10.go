package day10

import (
	"advent-of-code-2020/pkg/utils"
	"strings"
)

const (
	block = "#"
	space = "."
)

func SignalStrength(input []string) int {
	lines := utils.NoEmptyStrings(input)
	var cycle, signal int
	x := 1

	for i := range lines {
		cycle++

		if isSignalCycle(cycle) {
			signal += cycle * x
		}

		if lines[i][0] == 'a' {
			cycle++

			if isSignalCycle(cycle) {
				signal += cycle * x
			}

			x += utils.MustInt(lines[i][5:])
		}
	}
	return signal
}

func isSignalCycle(cycle int) bool {
	return cycle == 20 || cycle == 60 || cycle == 100 || cycle == 140 || cycle == 180 || cycle == 220
}

func CRTDraw(input []string) string {
	lines := utils.NoEmptyStrings(input)
	var cycle int
	var out string
	x := 1

	for i := range lines {
		out += blockOrSpace(x, cycle)
		cycle++

		if lines[i][0] == 'a' {
			out += blockOrSpace(x, cycle)
			cycle++

			x += utils.MustInt(lines[i][5:])
		}
	}

	var sb strings.Builder
	for i := 0; i < 40*6; i += 40 {
		sb.WriteString(out[i : i+40])
		sb.WriteRune('\n')
	}
	return sb.String()
}

func blockOrSpace(x, cycle int) string {
	if x >= (cycle%40)-1 && x <= (cycle%40)+1 {
		return block
	}
	return space
}
