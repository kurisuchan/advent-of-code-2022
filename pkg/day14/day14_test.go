package day14_test

import (
	"testing"

	"advent-of-code-2022/pkg/day14"
)

func TestSandOverflow(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"498,4 -> 498,6 -> 496,6\n503,4 -> 502,4 -> 502,9 -> 494,9", 24},
	}

	for _, test := range tests {
		actual := day14.SandOverflow(test.in)
		if actual != test.out {
			t.Errorf("SandOverflow(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestSandPile(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"498,4 -> 498,6 -> 496,6\n503,4 -> 502,4 -> 502,9 -> 494,9", 93},
	}

	for _, test := range tests {
		actual := day14.SandPile(test.in)
		if actual != test.out {
			t.Errorf("SandPile(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
