package day14

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"advent-of-code-2022/pkg/utils"
	"regexp"
	"strings"
)

func SandOverflow(input string) int {
	world, maxY := parseInput(input)
	start := utils2020.Coordinate2{X: 500, Y: 0}
	var settled int
outer:
	for {
		pos := start
		for {
			if !world[pos.Move(utils2020.South)] {
				pos = pos.Move(utils2020.South)
			} else if !world[pos.Move(utils2020.SouthWest)] {
				pos = pos.Move(utils2020.SouthWest)
			} else if !world[pos.Move(utils2020.SouthEast)] {
				pos = pos.Move(utils2020.SouthEast)
			} else {
				world[pos] = true
				settled++
				break
			}

			if pos.Y > maxY {
				break outer
			}
		}
	}

	return settled
}

func SandPile(input string) int {
	world, maxY := parseInput(input)
	start := utils2020.Coordinate2{X: 500, Y: 0}
	var settled int
	for {
		pos := start
		for {
			if !world[pos.Move(utils2020.South)] {
				pos = pos.Move(utils2020.South)
			} else if !world[pos.Move(utils2020.SouthWest)] {
				pos = pos.Move(utils2020.SouthWest)
			} else if !world[pos.Move(utils2020.SouthEast)] {
				pos = pos.Move(utils2020.SouthEast)
			} else {
				world[pos] = true
				settled++
				break
			}

			if pos.Y > maxY {
				world[pos] = true
				settled++
				break
			}
		}
		if world[start] {
			break
		}
	}

	return settled
}

func parseInput(input string) (map[utils2020.Coordinate2]bool, int) {
	pairs := regexp.MustCompile(`(\d+),(\d+)`)
	lines := strings.Split(input, "\n")

	var maxY int
	world := make(map[utils2020.Coordinate2]bool)

	for i := range lines {
		m := pairs.FindAllStringSubmatch(lines[i], -1)
		var coords []utils2020.Coordinate2
		for p := range m {
			coords = append(coords, utils2020.Coordinate2{X: utils2020.MustInt(m[p][1]), Y: utils2020.MustInt(m[p][2])})
		}
		for p := 0; p < len(coords)-1; p++ {
			for y := utils.Min(coords[p].Y, coords[p+1].Y); y <= utils.Max(coords[p].Y, coords[p+1].Y); y++ {
				if y > maxY {
					maxY = y
				}
				for x := utils.Min(coords[p].X, coords[p+1].X); x <= utils.Max(coords[p].X, coords[p+1].X); x++ {
					world[utils2020.Coordinate2{X: x, Y: y}] = true
				}
			}
		}
	}

	return world, maxY
}
