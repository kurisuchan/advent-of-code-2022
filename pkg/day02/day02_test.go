package day02_test

import (
	"testing"

	"advent-of-code-2022/pkg/day02"
)

func TestStrategy(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"A Y\nB X\nC Z\n", 15},
	}

	for _, test := range tests {
		actual := day02.Strategy(test.in)
		if actual != test.out {
			t.Errorf("Strategy(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestCorrectStrategy(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"A Y\nB X\nC Z\n", 12},
	}

	for _, test := range tests {
		actual := day02.CorrectStrategy(test.in)
		if actual != test.out {
			t.Errorf("CorrectStrategy(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestOutcome(t *testing.T) {
	tests := []struct {
		inA byte
		inB byte
		out int
	}{
		{'A', 'Y', 8},
		{'B', 'X', 1},
		{'C', 'Z', 6},
	}

	for _, test := range tests {
		actual := day02.Outcome(test.inA, test.inB)
		if actual != test.out {
			t.Errorf("Outcome(%q, %q) => %d, want %d", test.inA, test.inB, actual, test.out)
		}
	}
}

func TestOutcomeFixed(t *testing.T) {
	tests := []struct {
		inA byte
		inB byte
		out int
	}{
		{'A', 'Y', 4},
		{'B', 'X', 1},
		{'C', 'Z', 7},
		{'A', 'X', 3},
	}

	for _, test := range tests {
		actual := day02.OutcomeFixed(test.inA, test.inB)
		if actual != test.out {
			t.Errorf("OutcomeFixed(%q, %q) => %d, want %d", test.inA, test.inB, actual, test.out)
		}
	}
}
