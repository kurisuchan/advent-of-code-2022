package day02

import (
	"strings"
)

const (
	code      = 23
	scoreDraw = 3
	scoreWin  = 6
)

func Strategy(input string) int {
	rounds := strings.Split(input, "\n")
	var sum int
	for r := range rounds {
		if rounds[r] == "" {
			continue
		}
		sum += Outcome(rounds[r][0], rounds[r][2])
	}
	return sum
}

func CorrectStrategy(input string) int {
	rounds := strings.Split(input, "\n")
	var sum int
	for r := range rounds {
		if rounds[r] == "" {
			continue
		}
		sum += OutcomeFixed(rounds[r][0], rounds[r][2])
	}
	return sum
}

func Outcome(a, b byte) int {
	var score int
	if a == b-code {
		score = scoreDraw
	} else if a == b-code-1 || a == b-code+2 {
		score = scoreWin
	}
	return score + int(b-'W')
}

func OutcomeFixed(a, t byte) int {
	var move = (int(a-'A') + int(t) - int('Y')) % 3
	if move < 0 {
		move += 3
	}
	return 3*int(t-'X') + move + 1
}
