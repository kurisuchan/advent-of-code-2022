package day15

import (
	utils2020 "advent-of-code-2020/pkg/utils"
	"math"
	"regexp"
)

func NoBeacons(input string, bounds int) int {
	world := parseInput(input)

	minX := math.MaxInt
	maxX := math.MinInt
	for sensor, distance := range world.covered {
		if sensor.X-distance < minX {
			minX = sensor.X - distance
		}
		if sensor.X+distance > maxX {
			maxX = sensor.X + distance
		}
	}

	var covered int

	for x := minX; x <= maxX; x++ {
		cur := utils2020.Coordinate2{X: x, Y: bounds / 2}
		for sensor, distance := range world.covered {
			if cur.Distance(sensor) <= distance {
				s := skip(cur, sensor, distance)
				covered += s + 1
				x += s
				break
			}
		}
	}

	for beacon := range world.beacons {
		if beacon.Y == bounds/2 {
			covered--
		}
	}

	return covered
}

func DistressBeacon(input string, bounds int) int {
	world := parseInput(input)

	for y := 0; y <= bounds; y++ {
		for x := 0; x <= bounds; x++ {
			cur := utils2020.Coordinate2{X: x, Y: y}
			var found bool
			for sensor, distance := range world.covered {
				if cur.Distance(sensor) <= distance {
					found = true
					x += skip(cur, sensor, distance)
					break
				}
			}
			if !found {
				return x*4000000 + y
			}
		}
	}

	return 0
}

func skip(pos, sensor utils2020.Coordinate2, distance int) int {
	return (sensor.X - pos.X) + distance - utils2020.AbsInt(pos.Y-sensor.Y)
}

type World struct {
	beacons map[utils2020.Coordinate2]struct{}
	covered map[utils2020.Coordinate2]int
}

func parseInput(input string) World {
	w := World{
		covered: make(map[utils2020.Coordinate2]int),
		beacons: make(map[utils2020.Coordinate2]struct{}),
	}

	pattern := regexp.MustCompile(`Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)`)
	matches := pattern.FindAllStringSubmatch(input, -1)

	for i := range matches {
		sensor := utils2020.Coordinate2{X: utils2020.MustInt(matches[i][1]), Y: utils2020.MustInt(matches[i][2])}
		beacon := utils2020.Coordinate2{X: utils2020.MustInt(matches[i][3]), Y: utils2020.MustInt(matches[i][4])}

		w.covered[sensor] = sensor.Distance(beacon)
		w.beacons[beacon] = struct{}{}
	}

	return w
}
